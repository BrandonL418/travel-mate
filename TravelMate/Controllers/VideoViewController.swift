//
//  VideoViewController.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 3/28/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import UIKit
import WebKit
//import Flurry_iOS_SDK

class VideoViewController: UIViewController, WKNavigationDelegate {
    
    //MARK: - Properties and Outlets
    
    let drivers = DriverList.shared
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    //MARK: - App Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
//        Flurry.logEvent("Show Video Page")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        backButton.frame.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height * 6)
        guard let userID = defaults.string(forKey: "Videos") else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        print(userDriver)
        navigationController?.title = "Support Videos"
        webView.loadRequest(URLRequest(url: URL(string: userDriver.directLink)!))
    }
    
    //MARK: - Buttons
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

