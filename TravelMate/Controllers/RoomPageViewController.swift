import UIKit
import HomeKit

let defaults = UserDefaults.standard

enum AppPage: String, CaseIterable {
    case AccessoryViewController
    case RoomViewController
    case SceneKeypadViewController
}

class RoomPageViewController: UIPageViewController {
    
    //MARK: - Properties and Outlets
    
    let homeManager = HMHomeManager()
    var pages: [AppPage: UIViewController] = [:]
    
    //MARK: - App Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        dataSource = self
        loadPages()
        setPage(AppPage.RoomViewController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    //MARK: - Page Methods
    
    func setPage(_ page: AppPage) {
        guard let viewController = pages[page] else {
            return
        }
        setViewControllers([viewController],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }
    
    func loadPages() {
        let items = AppPage.allCases.compactMap(loadViewController)
        pages = Dictionary(uniqueKeysWithValues: items.map{ ($0.key, $0.controller) })
    }
    
    func loadViewController(_ page: AppPage) -> (key: AppPage, controller: UIViewController)? {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: page.rawValue)
        return (page, controller)
    }
}

extension RoomPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageNames = AppPage.allCases
        guard
            let key = pages.first(where: { $1 == viewController})?.key,
            let index = pageNames.firstIndex(of: key) else {
                return nil
        }
        let previousIndex = index - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard pages.count > previousIndex else {
            return nil
        }
        return pages[pageNames[previousIndex]]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageNames = AppPage.allCases
        guard
            let key = pages.first(where: { $1 == viewController})?.key,
            let index = pageNames.firstIndex(of: key) else {
                return nil
        }
        let nextIndex = index + 1
        let count = pages.count
        guard count != nextIndex else {
            return nil
        }
        guard count > nextIndex else {
            return nil
        }
        return pages[pageNames[nextIndex]]
    }
    
}
