//
//  AccessoryViewController.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 5/28/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import UIKit
import HomeKit
import SwiftDate
import anim
import SwiftyJSON
import CoreLocation
//import Flurry_iOS_SDK

class AccessoryViewController: UIViewController, SunModelDelegate, WeatherProcessorDelegate, CLLocationManagerDelegate, UISearchBarDelegate {
    
    func updateForecastUI() {
        for (index, cell) in weatherProcessor.weatherCells.enumerated() {
            cell.dayPartNameLabel.text = weatherProcessor.dayparts[index]
            cell.temperatureLabel.text = weatherProcessor.temps[index]
            cell.lowTempLabel.text = weatherProcessor.lowTemps[index]
            cell.precipitationLabel.text = weatherProcessor.rain[index]
            cell.image.image = UIImage(named: weatherProcessor.imageID[index])
        }
    }
    func updateCurrentTemp(temp: String) {
//        print(temp)
        tempButton.setTitle(temp, for: .normal)
        if thermostatLabel.text != nil {
            clockLabel.textAlignment = .left
            thermostatLabel.text = temp
        } else {
            clockLabel.textAlignment = .center
        }
    }
    func updateCurrentCity(city: String) {
        cityButton.setTitle(city, for: .normal)
    }
    func chooseResult(json: JSON, isHome: Bool) {
        if let arrayValue = json["location"]["address"].array {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for item in arrayValue.enumerated() {
            optionMenu.addAction(UIAlertAction(title: item.element.string!, style: .default, handler:{ action in
                
                print(json["location"]["address"][item.offset])
                self.weatherProcessor.prepareCall(lat: json["location"]["latitude"][item.offset].double!, lng: json["location"]["longitude"][item.offset].double!)
                self.sunModel.getSunData(lat: json["location"]["latitude"][item.offset].double!, lng: json["location"]["longitude"][item.offset].double!)
                if isHome {
                    self.defaults.set(true, forKey: "useDefault")
                    self.defaults.set(item.element.string!, forKey: "homeDefault")
                    print("home default set: \(self.defaults.string(forKey: "homeDefault")!)")
                }
                
                self.dismissSearch()
                AppStoreReviewManager.requestReviewIfAppropriate()
            }))
        }
        
            
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
        } else {
            print("Could not find matching results")
        }
    }
    
    func failedToRefresh() {
        print("yay")
        weather.isHidden = true
        errorLabel.isHidden = false
        refreshButton.isHidden = false
    }
    
    func updateWallpaper(timePeriod: String) {
//        print("update wallpaper: \(timePeriod)")
        roomImage.image = UIImage(named: timePeriod)
        noonLabel.text = "\(sunModel.twoDayTimes[2].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
        //        UVIndexLabel.text = ""
        sunriseLabel.text = "Sunrise: \(sunModel.twoDayTimes[0].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
        noonLabel.text = "\(sunModel.twoDayTimes[2].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
        sunsetLabel.text = "Sunset: \(sunModel.twoDayTimes[4].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
    }

    var drivers = DriverList.shared
    var localeModel = LocaleModel.shared
    
    //MARK: - Properties and Outlets
    
    @IBOutlet weak var tempatureLabel: UIButton!
    @IBOutlet weak var buttonBackground: UIView!
    @IBOutlet weak var logo: UIView!
    @IBOutlet weak var roomImage: UIImageView!
    @IBOutlet weak var clockLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var thermostatLabel: UILabel!
    @IBOutlet weak var screensaver: UIStackView!
    @IBOutlet weak var screensaverDismiss: UIButton!
    @IBOutlet weak var rightSwipeBar: UIView!
    @IBOutlet weak var leftSwipeBar: UIView!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var screensaverBottom: NSLayoutConstraint!
    @IBOutlet weak var screensaverCenter: NSLayoutConstraint!
    @IBOutlet weak var logoY: NSLayoutConstraint!
    @IBOutlet weak var logoX: NSLayoutConstraint!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var mateLogo: UILabel!
    @IBOutlet weak var dotLogo: UILabel!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var button9: UIButton!
    @IBOutlet weak var insta: UIButton!
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var cell1: ForecastCell!
    @IBOutlet weak var cell2: ForecastCell!
    @IBOutlet weak var cell3: ForecastCell!
    @IBOutlet weak var noonLabel: UILabel!
    @IBOutlet weak var weather: UIView!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var tempButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    
    let defaults = UserDefaults.standard
    let sunModel = SunModel.shared
    let weatherProcessor = WeatherProcessor.shared
    var locationManager: CLLocationManager?
    var scenes: [HMActionSet] = []
    var currentRoom : HMRoom?
    var blurAnimator: UIViewPropertyAnimator!
    var timer = Timer()
    var temperatureTimer = Timer()
    var sceneTimer = Timer()
    var temperature = 0
    var sceneCooldown = true
    var screensaverOn = false
    var SSCounter = 0
    var selectedBrand = "mate" {
        didSet {
            if defaults.string(forKey: "Videos") == "Advanced" {
                if defaults.string(forKey: "HomeControl") == "Lutron" {
//                    print("SELECTED BRAND: lutron")
                    changeLogo(brand: "lutron")
                } else if defaults.string(forKey: "HomeControl") == "Philips Hue" {
//                    print("hue")
                    changeLogo(brand: "hue")
                } else {
//                    print("default logo")
                    changeLogo(brand: "mate")
                }
            } else {
//                print("default log oof")
                changeLogo(brand: "mate")
            }
        }
    }

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: - Buttons
    
    @IBAction func IGPressed(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
//        Flurry.logEvent("Instagram Pressed")
        generator.impactOccurred()
        if selectedBrand == "lutron" {
            if let url = URL(string: "https://instagram.com/lutronelectronics?igshid=11upo3e4chxyg"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
        } else if selectedBrand == "hue" {
            if let url = URL(string: "https://instagram.com/philipshue?igshid=3xfaz3vqu5xs"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
        } else {
            if let url = URL(string: "https://www.instagram.com/homekitsmate/"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
        }
    }
    @IBAction func button6(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button6") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func button7(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button7") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func button8(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button8") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func button9(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button9") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func amazonLinkPressed(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
//        Flurry.logEvent("Plus Button Pressed")
        if selectedBrand == "lutron" {
            print("Lutron")
            if let url = URL(string: "https://www.amazon.com/stores/node/2595825011?_encoding=UTF8&field-lbr_brands_browse-bin=Lutron&ref_=bl_dp_s_web_2595825011"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
            return
        } else if selectedBrand == "hue" {
            print("Philips Hue")
            if let url = URL(string: "https://www.amazon.com/stores/node/18011227011?_encoding=UTF8&field-lbr_brands_browse-bin=Philips%20Hue&ref_=bl_dp_s_web_18011227011"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
            return
        }
        let title = "Add devices tested by mate."
        let deviceButtons : [String] = ["Philips Hue Bulbs", "Honeywell Thermostat", "Smart Lock", "MyQ Garage", "Apple TV", "Sprinkler Controller"]
        
        let optionMenu = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        optionMenu.addAction(UIAlertAction(title: "mate. merch", style: .default, handler:{ action in
            if let url = URL(string: "https://shop.spreadshirt.com/mate1/all"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
        }))
        for device in deviceButtons.enumerated() {
            optionMenu.addAction(UIAlertAction(title: device.element, style: .default, handler:{ action in
                self.showDevice(device: action.title!, index: device.offset)
            }))
        }
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    @IBAction func cityButtonPressed(_ sender: Any) {
        search()
    }
    @IBAction func refreshButtonPressed(_ sender: Any) {
        weather.isHidden = false
        errorLabel.isHidden = true
        refreshButton.isHidden = true
        appDelegate.setupLocationManager()
    }
    @objc func normalTap(_ sender: UIGestureRecognizer){
        print("Normal tap")
        //label.text = "Normal Tap"
    }
    @objc func longTap(_ sender: UIGestureRecognizer){
        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            
            if sender.view as? UIButton == plusButton {
                print("change country")
                let regions = localeModel.regions.sorted() { $0 < $1 }
                let optionMenu = UIAlertController(title: "Change Country", message: nil, preferredStyle: .actionSheet)
                for item in regions {
                    optionMenu.addAction(UIAlertAction(title: item.key, style: .default, handler: changeRegion(action:)))
                }
                optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                if let popoverController = optionMenu.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                }
                self.present(optionMenu, animated: true, completion: nil)
            } else
            if let button = sender.view as? UIButton,
                let position = buttonStack.arrangedSubviews.firstIndex(where: {$0 == button}) {
                print(position)
                
                if position >= 2 {
                    let optionMenu = UIAlertController(title: "Category", message: "Choose which category you would like this button to control", preferredStyle: .actionSheet)
                    optionMenu.addAction(UIAlertAction(title: "Pool", style: .default, handler:{ action in
                        self.addMenu(position: 3, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Weather", style: .default, handler:{ action in
                        self.addMenu(position: 4, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Garage", style: .default, handler:{ action in
                        self.addMenu(position: 2, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                    if let popoverController = optionMenu.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    self.present(optionMenu, animated: true, completion: nil)
                } else {
                    addMenu(position: position)
                }
            } else {
                chooseLocation()
            }
        }
    }
    func addMenu(position: Int, button: Int = 0) {
        let title = accessoryCategory(rawValue: position)?.description
        
        let optionMenu = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        for item in drivers.returnDrivers(for: .accessories).filter({$0.type.rawValue == title}).sorted(by: {$0.id < $1.id}) {
            optionMenu.addAction(UIAlertAction(title: item.id, style: .default, handler:{ action in
                
                if button == 2 {
                    self.defaults.set(action.title!, forKey: title!)
                    let articleParams = ["Driver": action.title!, "Category" : title!]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                    self.defaults.set(title, forKey: "button8")
                    self.defaults.set(title, forKey: "button8image")
                    self.button8.setBackgroundImage(UIImage(named: self.defaults.string(forKey: "button8image")!), for: .normal)
                } else if button == 3 {
                    let title2 = "\(title!)2"
                    self.defaults.set(action.title!, forKey: title2)
                    let articleParams = ["Driver": action.title!, "Category" : title2]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                    self.defaults.set(title2, forKey: "button9")
                    self.defaults.set(title, forKey: "button9image")
                    self.button9.setBackgroundImage(UIImage(named: self.defaults.string(forKey: "button9image")!), for: .normal)
                } else {
                    self.defaults.set(action.title!, forKey: title!)
                    let articleParams = ["Driver": action.title!, "Category" : title!]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                }
            }))
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    func showDevice(device: String, index: Int = 0) {
        let key = "device\(index + 1)Link"
        print(device)
        print(key)
        if let url = URL(string: defaults.string(forKey: key)!),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: "https://www.homekitsmate.com/devices"),
            UIApplication.shared.canOpenURL(url) {
            print(defaults.string(forKey: "amazon")!)
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        
    }
    func changeRegion(action: UIAlertAction) {
        guard let name = action.title else {
            return
        }
        let regionID = localeModel.regions[name]!
        let region = localeModel.updateLinkCountry(region: regionID)
        defaults.set(region["link"]!, forKey: "amazon")
        defaults.set(region["roomText"]!, forKey: "roomText")
        defaults.set(region["shadeText"]!, forKey: "shadeText")
        defaults.set(region["demoTitle"]!, forKey: "demoTitle")
        defaults.set(region["device1Link"], forKey: "device1Link")
        defaults.set(region["device2Link"], forKey: "device2Link")
        defaults.set(region["device3Link"], forKey: "device3Link")
        defaults.set(region["device4Link"], forKey: "device4Link")
        defaults.set(region["device5Link"], forKey: "device5Link")
        defaults.set(region["device6Link"], forKey: "device6Link")
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
//        print(textSearched)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print(searchBar.text as Any)
        searchBar.resignFirstResponder()
        weatherProcessor.weatherDelegate = self
        weatherProcessor.searchWeather(search: searchBar.text!, sender: "searchBar")
    }
    func chooseLocation() {
        let optionMenu = UIAlertController(title: nil, message: "Home: \(defaults.string(forKey: "homeDefault") ?? "")", preferredStyle: .actionSheet)
        optionMenu.addAction(UIAlertAction(title: "Current Location", style: .default, handler:{ action in
            self.defaults.set(false, forKey: "useDefault")
            self.getCurrentLocation()
            self.dismissSearch()
        }))
        optionMenu.addAction(UIAlertAction(title: "Home", style: .default, handler:{ action in
            if self.defaults.string(forKey: "homeDefault") != nil {
                self.defaults.set(true, forKey: "useDefault")
                self.weatherProcessor.searchWeather(search: self.defaults.string(forKey: "homeDefault")!, sender: "")
                self.dismissSearch()
            } else {
                self.changeDefaultLocation()
            }
        }))
        optionMenu.addAction(UIAlertAction(title: "Change Home Location", style: .default, handler:{ action in
            self.changeDefaultLocation()
        }))
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    @objc func search() {
        let blurEffectView = UIVisualEffectView()
        blurEffectView.tag = 100
        blurEffectView.backgroundColor = .clear
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let dismissButton = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        dismissButton.addTarget(self, action: #selector(dismissSearch), for: .touchUpInside)
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: (view.frame.width/2) - 100, width: view.frame.width, height: 30))
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = "Ex: Paris, France"
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        let currentLocationButton = UIButton(frame: CGRect(x: 10, y: 20, width: 50, height: 50))
        currentLocationButton.addTarget(self, action: #selector(getCurrentLocation), for: .touchUpInside)
        currentLocationButton.setImage(UIImage(named: "noun_Location_2565177-2"), for: .normal)
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        currentLocationButton.addGestureRecognizer(longGesture1)
        blurEffectView.contentView.addSubview(dismissButton)
        blurEffectView.contentView.addSubview(searchBar)
        blurEffectView.contentView.addSubview(currentLocationButton)
        view.addSubview(blurEffectView)
        
        blurAnimator = UIViewPropertyAnimator(duration: 1, curve: .linear) { [blurEffectView] in
            blurEffectView.effect = UIBlurEffect(style: .light)
        }
        
        blurAnimator.fractionComplete = 0.15 // set the blur intensity.
    }
    @objc func getCurrentLocation() {
        appDelegate.setupLocationManager()
        dismissSearch()
        AppStoreReviewManager.requestReviewIfAppropriate()
    }
    func changeDefaultLocation() {
        let changeHome = UIAlertController(title: "Change Home Location", message: "To change what location mate defaults to for weather, search for your home here.", preferredStyle: .alert)
        changeHome.addTextField { (textField) in
            textField.placeholder = "Ex: 73116"
        }
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        changeHome.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak changeHome] (_) in
            let textField = changeHome!.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(String(describing: textField.text))")
//            changeHome?.dismiss(animated: true, completion: nil)
            if let text = textField.text {
                self.weatherProcessor.searchWeather(search: text, sender: "homeSearchBar")
            }
        }))
        
        // 4. Present the alert.
        self.present(changeHome, animated: true, completion: nil)
    }
    func changeLogo(brand: String) {
        if brand == "mate" {
            mateLogo.isHidden = false
            dotLogo.isHidden = false
            logoImage.isHidden = true
        } else {
            mateLogo.isHidden = true
            dotLogo.isHidden = true
            logoImage.isHidden = false
            logoImage.image = UIImage(named: brand)
        }
    }
    func checkLogo() {
        if defaults.string(forKey: "Videos") == "Advanced" {
            if defaults.string(forKey: "HomeControl") == "Lutron" {
//                print("Lutron")
                selectedBrand = "lutron"
            } else if defaults.string(forKey: "HomeControl") == "Philips Hue" {
//                print("Philips Hue")
                selectedBrand = "hue"
            } else {
//                print("default logo")
                selectedBrand = "mate"
            }
        } else {
//            print("default logo")
            selectedBrand = "mate"
        }
    }
    
    @objc func dismissSearch() {
        if let viewWithTag = self.view.viewWithTag(100) {
//            print("Tag 100")
            viewWithTag.removeFromSuperview()
        } else {
//            print("tag not found")
        }
    }
    
    //MARK: - App Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
//        Flurry.logEvent("Show Weather Page")
        roomImage.image = UIImage(named: sunModel.twoDayTimes[sunModel.currentTimePeriod].timePeriod)
        showBars()
        screensaverDismiss(self)
        checkLogo()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLogo()
        NotificationCenter.default.addObserver(self, selector: #selector(activateScreensaver), name: Notification.Name.TimeOutUserInteraction, object: nil)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
        temperatureTimer = Timer.scheduledTimer(timeInterval: 900, target: self, selector: #selector(self.weatherRefresh), userInfo: nil, repeats: true)
        sunModel.delegate = self
        weatherProcessor.weatherObservers.append(self)
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture4 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture5 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture6 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture7 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        button6.addGestureRecognizer(longGesture1)
        button7.addGestureRecognizer(longGesture2)
        button8.addGestureRecognizer(longGesture3)
        button9.addGestureRecognizer(longGesture4)
        plusButton.addGestureRecognizer(longGesture5)
        cityButton.addGestureRecognizer(longGesture6)
        tempButton.addGestureRecognizer(longGesture7)
        button6.setBackgroundImage(UIImage(named: defaults.string(forKey: "button6")!), for: .normal)
        button7.setBackgroundImage(UIImage(named: defaults.string(forKey: "button7")!), for: .normal)
        button8.setBackgroundImage(UIImage(named: defaults.string(forKey: "button8image")!), for: .normal)
        button9.setBackgroundImage(UIImage(named: defaults.string(forKey: "button9image")!), for: .normal)
        
        weatherProcessor.weatherDelegate = self
        sunModel.observers.append(self)
        
        weatherProcessor.weatherCells.append(cell1)
        weatherProcessor.weatherCells.append(cell2)
        weatherProcessor.weatherCells.append(cell3)
        weatherProcessor.addedCells()
        
        weekdayLabel.text = "\(Date().weekdayName(.default)), \(Date().date.in(region: Region.current).toFormat("MMM d", locale: Locale.current))"
        sunriseLabel.text = "Sunrise: \(sunModel.twoDayTimes[0].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
        noonLabel.text = "\(sunModel.twoDayTimes[2].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
        sunsetLabel.text = "Sunset: \(sunModel.twoDayTimes[4].date.date.in(region: Region.current).toFormat("h:mm a", locale: Locale.current))"
//        let date = DateInRegion().toFormat("M/dd/yy")
        let date = DateInRegion().convertTo(region: Region.current)
//        print(date)
        
        cityButton.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        cityButton.titleLabel?.numberOfLines = 2
//        cityButton.setTitle("Weather", for: .normal)
    }
    override func viewDidDisappear(_ animated: Bool) {
        screensaverDismiss(self)
    }
    
    //MARK: - Timers
    
    @objc func tick() {
        clockLabel.text = DateFormatter.localizedString(from: Date(),
                                                        dateStyle: .none,
                                                        timeStyle: .short)
        dateLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .none)
        
            bars += 1
            if bars >= 3 {
                hideBars()
            }
        
        if screensaverOn && SSCounter >= 60 {
            SSCounter = 0
            animScreensaver()
        } else {
            SSCounter += 1
        }
        button6.setBackgroundImage(UIImage(named: defaults.string(forKey: "button6")!), for: .normal)
        button7.setBackgroundImage(UIImage(named: defaults.string(forKey: "button7")!), for: .normal)
        button8.setBackgroundImage(UIImage(named: defaults.string(forKey: "button8image")!), for: .normal)
        button9.setBackgroundImage(UIImage(named: defaults.string(forKey: "button9image")!), for: .normal)
    }
    @objc func weatherRefresh() {
        appDelegate.setupLocationManager()
    }
    
    //MARK: - Screensaver Methods
    
    @objc func activateScreensaver() {
        checkLogo()
        if !screensaverDisabled {
            if let temperature = weatherProcessor.temperature {
                updateCurrentTemp(temp: temperature)
            }
            buttonBackground.isHidden = true
            screensaver.isHidden = false
            screensaverDismiss.isHidden = false
            plusButton.isHidden = true
            insta.isHidden = true
            screensaverOn = true
            weather.isHidden = true
            errorLabel.isHidden = true
            refreshButton.isHidden = true
            logo.isHidden = false
        }
    }
    @IBAction func screensaverDismiss(_ sender: Any) {
        buttonBackground.isHidden = false
        screensaver.isHidden = true
        screensaverDismiss.isHidden = true
        leftSwipeBar.isHidden = false
        rightSwipeBar.isHidden = false
        plusButton.isHidden = false
        insta.isHidden = false
        screensaverOn = false
        weather.isHidden = weatherProcessor.broken
        errorLabel.isHidden = !weatherProcessor.broken
        refreshButton.isHidden = !weatherProcessor.broken
        logo.isHidden = true
    }
    func animScreensaver() {
        anim(constraintParent: self.view) { (settings) -> (animClosure) in
            settings.duration = 30
            settings.ease = .easeInOutQuad
            return {
                self.screensaverBottom.constant = 53
                self.screensaverCenter.constant = 1
                self.logoX.constant = -1
                if self.traitCollection.horizontalSizeClass == .compact {
                    self.logoY.constant = -21
                } else {
                    self.logoY.constant = -41
                }
            }
            }.then (constraintParent: self.view) { (settings) -> (animClosure) in
                settings.duration = 30
                settings.ease = .easeInOutQuad
                return {
                    self.screensaverBottom.constant = 52
                    self.screensaverCenter.constant = -1
                    self.logoX.constant = 1
                    if self.traitCollection.horizontalSizeClass == .compact {
                        self.logoY.constant = -19
                    } else {
                        self.logoY.constant = -39
                    }
                }
        }
    }
    func timeDateFormat() {
        let date = Date()
        let customFormat = "ddMMMM"
        let gbLocale = Locale(identifier: "en_GB")
        let ukFormat = DateFormatter.dateFormat(fromTemplate: customFormat, options: 0, locale: gbLocale)
        let usLocale = Locale(identifier: "en_US")
        let usFormat = DateFormatter.dateFormat(fromTemplate: customFormat, options: 0, locale: usLocale)
        let formatter = DateFormatter()
        formatter.dateFormat = ukFormat
        formatter.string(from: date)
        formatter.dateFormat = usFormat
        formatter.string(from: date)
    }
    
    //MARK: - Bars
    
    var bars = 0
    func showBars() {
        leftSwipeBar.isHidden = false
        rightSwipeBar.isHidden = false
        bars = 0
    }
    func hideBars() {
        leftSwipeBar.isHidden = true
        rightSwipeBar.isHidden = true
    }
}

enum accessoryCategory : Int {
    case Camera
    case Car
    case Garage
    case Pool
    case Weather
    
    var description: String {
        switch self {
        case .Camera:
            return "Camera"
        case .Car:
            return "Car"
        case .Garage:
            return "Garage"
        case .Pool:
            return "Pool"
        case .Weather:
            return "Weather"
        }
    }
}
