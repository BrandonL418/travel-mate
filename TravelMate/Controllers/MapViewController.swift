import AVFoundation
import HomeKit
import UIKit
import anim
//import Flurry_iOS_SDK

class MapViewController: UIViewController, SunModelDelegate, WeatherProcessorDelegate {
    
    func updateCurrentTemp(temp: String) {
        if temperatureLabel.text != nil {
            clockLabel.textAlignment = .left
            temperatureLabel.text = temp
        } else {
            clockLabel.textAlignment = .center
        }
    }
    
    @IBOutlet weak var keypadWallpaper: UIImageView!
    @IBOutlet weak var leftSwipeBar: UIView!
    @IBOutlet weak var clockLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var screensaver: UIStackView!
    @IBOutlet weak var insta: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var button10: UIButton!
    @IBOutlet weak var button11: UIButton!
    @IBOutlet weak var button12: UIButton!
    @IBOutlet weak var button13: UIButton!
    @IBOutlet weak var logo: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var mateLogo: UIView!
    @IBOutlet weak var dotLogo: UILabel!
    @IBOutlet weak var screensaverDismiss: UIButton!
    @IBOutlet weak var screensaverBottom: NSLayoutConstraint!
    @IBOutlet weak var screensaverCenter: NSLayoutConstraint!
    @IBOutlet weak var logoX: NSLayoutConstraint!
    @IBOutlet weak var logoY: NSLayoutConstraint!
    @IBOutlet weak var driverButtons: UIStackView!
    
    private let screenSize = UIScreen.main.bounds
    private var player: AVAudioPlayer?
    let localeModel = LocaleModel()
    let sunModel = SunModel.shared
    var timer = Timer()
    var screensaverOn = false
    var SSCounter = 0
    var isiPad = false

    let drivers = DriverList.shared
    let defaults = UserDefaults.standard
    let weatherProcessor = WeatherProcessor.shared
    
    //MARK: - App Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        keypadWallpaper.image = UIImage(named: sunModel.twoDayTimes[sunModel.currentTimePeriod].timePeriod)
        
        showBars()
        screensaverDismiss(self)
    }
    override func viewDidLoad() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            //demoButtons = ["sunrise", "morning", "noon", "afternoon", "evening"]
            isiPad = true
        }
        super.viewDidLoad()
        sunModel.delegate = self
        sunModel.findTimePeriod()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(activateScreensaver), name: Notification.Name.TimeOutUserInteraction, object: nil)
        
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture4 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture5 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        button10.addGestureRecognizer(longGesture1)
        button11.addGestureRecognizer(longGesture2)
        button12.addGestureRecognizer(longGesture3)
        button13.addGestureRecognizer(longGesture4)
        plusButton.addGestureRecognizer(longGesture5)
        button10.setBackgroundImage(UIImage(named: defaults.string(forKey: "button11")!), for: .normal)
        button11.setBackgroundImage(UIImage(named: defaults.string(forKey: "button12")!), for: .normal)
        button12.setBackgroundImage(UIImage(named: defaults.string(forKey: "button13image")!), for: .normal)
        button13.setBackgroundImage(UIImage(named: defaults.string(forKey: "button14image")!), for: .normal)
        sunModel.observers.append(self)
//        weatherProcessor.weatherDelegate = self
        weatherProcessor.addedCells()
        weatherProcessor.weatherObservers.append(self)
    }
    
    //MARK: - Timers
    
    @objc func tick() {
        
            bars += 1
            if bars >= 3 {
                hideBars()
            }
        
        clockLabel.text = DateFormatter.localizedString(from: Date(),
                                                        dateStyle: .none,
                                                        timeStyle: .short)
        dateLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .none)
        
        if screensaverOn && SSCounter >= 60 {
            SSCounter = 0
            animScreensaver()
        } else {
            SSCounter += 1
        }
        
        button10.setBackgroundImage(UIImage(named: defaults.string(forKey: "button11")!), for: .normal)
        if button11.currentTitle == nil {
            button11.setBackgroundImage(UIImage(named: defaults.string(forKey: "button12")!), for: .normal)
        } else {
            button11.setBackgroundImage(UIImage(named: "emptyThermostat"), for: .normal)
        }
        button12.setBackgroundImage(UIImage(named: defaults.string(forKey: "button13image")!), for: .normal)
        button13.setBackgroundImage(UIImage(named: defaults.string(forKey: "button14image")!), for: .normal)
    }
    
    //MARK: - Screensaver Methods
    @objc func activateScreensaver() {
        if let temperature = weatherProcessor.temperature {
            updateCurrentTemp(temp: temperature)
        }
        if !screensaverDisabled {
            screensaverOn = true
            screensaver.isHidden = false
         //   facebook.isHidden = true
            insta.isHidden = true
            plusButton.isHidden = true
            logo.isHidden = false
            screensaverDismiss.isHidden = false
            button10.isHidden = true
            button11.isHidden = true
            button12.isHidden = true
            button13.isHidden = true
        }
    }
    @IBAction func screensaverDismiss(_ sender: Any) {
        screensaverOn = false
        screensaver.isHidden = true
       // facebook.isHidden = false
        insta.isHidden = false
        plusButton.isHidden = false
        logo.isHidden = true
        screensaverDismiss.isHidden = true
        button10.isHidden = false
        button11.isHidden = false
        button12.isHidden = false
        button13.isHidden = false
    }
    func animScreensaver() {
        anim(constraintParent: self.view) { (settings) -> (animClosure) in
            settings.duration = 30
            settings.ease = .easeInOutQuad
            return {
                self.screensaverBottom.constant = 53
                self.screensaverCenter.constant = 1
                self.logoX.constant = -1
                if self.traitCollection.horizontalSizeClass == .compact {
                    self.logoY.constant = -21
                } else {
                    self.logoY.constant = -41
                }
            }
        }.then (constraintParent: self.view) { (settings) -> (animClosure) in
            settings.duration = 30
            settings.ease = .easeInOutQuad
            return {
                self.screensaverBottom.constant = 52
                self.screensaverCenter.constant = -1
                self.logoX.constant = 1
                if self.traitCollection.horizontalSizeClass == .compact {
                    self.logoY.constant = -19
                } else {
                    self.logoY.constant = -39
                }
            }
        }
    }

    func updateWallpaper(timePeriod: String) {
        keypadWallpaper.image = UIImage(named: timePeriod)
    }
    
    //MARK: - Bars
    
    var bars = 0
    func showBars() {
        leftSwipeBar.isHidden = false
        bars = 0
    }
    func hideBars() {
        leftSwipeBar.isHidden = true
    }
    
    //MARK: - Constant Buttons
    
    @IBAction func amazonLinkPressed(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        let title = "Add devices tested by mate."
        let deviceButtons : [String] = ["Philips Hue Bulbs", "Honeywell Thermostat", "Smart Lock", "MyQ Garage", "Apple TV", "Sprinkler Controller"]
        let optionMenu = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        optionMenu.addAction(UIAlertAction(title: "mate. merch", style: .default, handler:{ action in
            if let url = URL(string: "https://shop.spreadshirt.com/mate1/all"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
        }))
        for device in deviceButtons.enumerated() {
            optionMenu.addAction(UIAlertAction(title: device.element, style: .default, handler:{ action in
                self.showDevice(device: action.title!, index: device.offset)
            }))
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    func showDevice(device: String, index: Int = 0) {
        let key = "device\(index + 1)Link"
//        print(device)
//        print(key)
        if let url = URL(string: defaults.string(forKey: key)!),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: "https://www.homekitsmate.com/devices"),
            UIApplication.shared.canOpenURL(url) {
//            print(defaults.string(forKey: "amazon")!)
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        
    }
    @IBAction func instagramPressed(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
            if let url = URL(string: "https://www.instagram.com/homekitsmate/"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            
        }
    }
    @IBAction func button10(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button11") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func button11(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button12") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func button12(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button13") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    @IBAction func button13(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        guard let link = defaults.string(forKey: "button14") else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        //        print(defaults.string(forKey: "shade")!)
    }
    
    @objc func longTap(_ sender: UIGestureRecognizer){
//        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            
            if sender.view as? UIButton == plusButton {
                print("change country")
                let regions = localeModel.regions.sorted() { $0 < $1 }
                let optionMenu = UIAlertController(title: "Change Country", message: nil, preferredStyle: .actionSheet)
                for item in regions {
                    optionMenu.addAction(UIAlertAction(title: item.key, style: .default, handler: changeRegion(action:)))
                }
                optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                if let popoverController = optionMenu.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                }
                self.present(optionMenu, animated: true, completion: nil)
            } else
            if let button = sender.view as? UIButton,
                let position = driverButtons.arrangedSubviews.firstIndex(where: {$0 == button}) {
                print(position)
                
                if position >= 2 {
                    let optionMenu = UIAlertController(title: "Category", message: "Choose which category you would like this button to control", preferredStyle: .actionSheet)
                    optionMenu.addAction(UIAlertAction(title: "Lock", style: .default, handler: {action in
                        self.addMenu(position: 2, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Appliances", style: .default, handler: {action in
                        self.addMenu(position: 3, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Pets", style: .default, handler:{ action in
                        self.addMenu(position: 4, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Plants", style: .default, handler:{ action in
                        self.addMenu(position: 5, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Baby Monitors", style: .default, handler:{ action in
                        self.addMenu(position: 6, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Shades", style: .default, handler:{ action in
                        self.addMenu(position: 7, button: position)
                    }))
                    optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                    if let popoverController = optionMenu.popoverPresentationController {
                        popoverController.sourceView = self.view
                        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                        popoverController.permittedArrowDirections = []
                    }
                    self.present(optionMenu, animated: true, completion: nil)
                
                } else {
                    self.addMenu(position: position, button: position)
                }
            }
        }
    }
    func addMenu(position: Int, button: Int = 0) {
        
        let title = keypadCategory(rawValue: position)?.description
        var displayTitle = title
        
        if title == "BabyMonitors" {
            displayTitle = "Baby Monitors"
        } else if title == "Shade" {
            displayTitle = "Shades and Lighting"
        }
        
        var sourcePage = Page.keypad
        
        if button <= 1 {
            sourcePage = .rooms
        }
        
        let optionMenu = UIAlertController(title: displayTitle, message: nil, preferredStyle: .actionSheet)
        
        for item in drivers.returnDrivers(for: sourcePage).filter({$0.type.rawValue == title}).sorted(by: {$0.id < $1.id}) {
            let itemTitle = item.id
            var itemDisplayTitle = itemTitle
            if itemTitle == "Shades" {
                itemDisplayTitle = "Shades and Lighting"
            }
            optionMenu.addAction(UIAlertAction(title: itemDisplayTitle, style: .default, handler:{ action in
                
                if button == 0 {
                    self.defaults.set(action.title!, forKey: title!)
                    let articleParams = ["Driver": action.title!, "Category" : title!]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                    self.defaults.set(title, forKey: "button11")
                    self.defaults.set(title, forKey: "button11image")
                    self.button10.setBackgroundImage(UIImage(named: self.defaults.string(forKey: "button11image")!), for: .normal)
                } else if button == 1 {
                    self.defaults.set(action.title!, forKey: title!)
                    let articleParams = ["Driver": action.title!, "Category" : title!]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                    self.defaults.set(title, forKey: "button12")
                    self.defaults.set(title, forKey: "button12image")
                    self.button11.setBackgroundImage(UIImage(named: self.defaults.string(forKey: "button12image")!), for: .normal)
                } else if button == 2 {
                    self.defaults.set(action.title!, forKey: title!)
                    let articleParams = ["Driver": action.title!, "Category" : title!]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                    self.defaults.set(title, forKey: "button13")
                    self.defaults.set(title, forKey: "button13image")
                    self.button12.setBackgroundImage(UIImage(named: self.defaults.string(forKey: "button13image")!), for: .normal)
                } else if button == 3 {
                    let title2 = "\(title!)2"
                    self.defaults.set(action.title!, forKey: title2)
//                    let articleParams = ["Driver": action.title!, "Category" : title2]
//                    Flurry.logEvent("Driver Changed", withParameters: articleParams)
                    self.defaults.set(title2, forKey: "button14")
                    self.defaults.set(title, forKey: "button14image")
                    self.button13.setBackgroundImage(UIImage(named: self.defaults.string(forKey: "button14image")!), for: .normal)
                } else {
                    self.defaults.set(action.title!, forKey: title!)
                }
            }))
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    func changeRegion(action: UIAlertAction) {
        guard let name = action.title else {
            return
        }
        let regionID = localeModel.regions[name]!
        let region = localeModel.updateLinkCountry(region: regionID)
        defaults.set(region["link"]!, forKey: "amazon")
        defaults.set(region["roomText"]!, forKey: "roomText")
        defaults.set(region["shadeText"]!, forKey: "shadeText")
        defaults.set(region["demoTitle"]!, forKey: "demoTitle")
        defaults.set(region["device1Link"], forKey: "device1Link")
        defaults.set(region["device2Link"], forKey: "device2Link")
        defaults.set(region["device3Link"], forKey: "device3Link")
        defaults.set(region["device4Link"], forKey: "device4Link")
        defaults.set(region["device5Link"], forKey: "device5Link")
        defaults.set(region["device6Link"], forKey: "device6Link")
    }
}

enum keypadCategory : Int {
    case Security
    case Thermostat
    case Lock
    case Appliances
    case Pets
    case Plants
    case BabyMonitors
    case Shade
    var description: String {
        switch self {
        case .Security:
            return "Security"
        case .Thermostat:
            return "Thermostat"
        case .Lock:
            return "Lock"
        case .Appliances:
            return "Appliances"
        case .Pets:
            return "Pets"
        case .Plants:
            return "Plants"
        case .BabyMonitors:
            return "BabyMonitors"
        case .Shade:
            return "Shade"
        
        }
    }
}
