//
//  NewDevicesViewController.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 7/24/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import UIKit
import HomeKit

class NewDevicesViewController: UITableViewController {
    
    let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    var accessories: [HMAccessory] = []
    var home: HMHome?
    
    // For discovering new accessories
    let browser = HMAccessoryBrowser()
    var discoveredAccessories: [HMAccessory] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(type: .custom)
        //set image for button
        button.setTitle("Find Devices", for: .normal)
        //add function for button
        button.addTarget(self, action: #selector(discoverDevices), for: .touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 51)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton

        // Do any additional setup after loading the view.
    }
    
    @IBAction func pressed(_ sender: Any) {
        discoverDevices()
    }
    

    @objc func discoverDevices() {
//        activityIndicator.startAnimating()
//        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)

        discoveredAccessories.removeAll()
        browser.delegate = self
        browser.startSearchingForNewAccessories()
        perform(#selector(stopDiscoveringAccessories), with: nil, afterDelay: 10)
    }
    
    @objc private func stopDiscoveringAccessories() {
    //    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(discoverAccessories(sender:)))
        if discoveredAccessories.isEmpty {
            let alert = UIAlertController(title: "No Accessories Found",
                                          message: "No Accessories were found. Make sure your accessory is nearby and on the same network.",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        } else {
            let homeName = home?.name
            let message = """
            Found a total of \(discoveredAccessories.count) accessories. \
            Add them to your home \(homeName ?? "")?
            """
            
            let alert = UIAlertController(
                title: "Accessories Found",
                message: message,
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default))
            alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
                self.addAccessories(self.discoveredAccessories)
            })
            present(alert, animated: true)
        }
    }
    
    private func addAccessories(_ accessories: [HMAccessory]) {
        for accessory in accessories {
            home?.addAccessory(accessory) { [weak self] error in
                guard let self = self else {
                    return
                }
                if let error = error {
                    print("Failed to add accessory to home: \(error.localizedDescription)")
                } else {
                    self.loadAccessories()
                }
            }
        }
    }
    
    private func loadAccessories() {
        guard let homeAccessories = home?.accessories else {
            return
        }
        
        for accessory in homeAccessories {
            if let characteristic = accessory.find(serviceType: HMServiceTypeLightbulb, characteristicType: HMCharacteristicMetadataFormatBool) {
                accessories.append(accessory)
                accessory.delegate = self
                characteristic.enableNotification(true) { error in
                    if error != nil {
                        print("Something went wrong when enabling notification for a chracteristic. \(String(describing: error))")
                    }
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewDevicesViewController: HMAccessoryDelegate {
    func accessory(_ accessory: HMAccessory, service: HMService, didUpdateValueFor characteristic: HMCharacteristic) {
       // collectionView?.reloadData()
    }
}

extension NewDevicesViewController: HMAccessoryBrowserDelegate {
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didFindNewAccessory accessory: HMAccessory) {
        discoveredAccessories.append(accessory)
    }
}

extension HMAccessory {
    func find(serviceType: String, characteristicType: String) -> HMCharacteristic? {
        return services.lazy
            .filter { $0.serviceType == serviceType }
            .flatMap { $0.characteristics }
            .first { $0.metadata?.format == characteristicType }
    }
}
