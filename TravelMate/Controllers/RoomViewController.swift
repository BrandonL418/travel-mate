import UIKit
import HomeKit
import SwiftDate
import anim

//MARK: - Global Variables

var roomIndex = 0
var screensaverDisabled = false

class RoomViewController: UIViewController, SunModelDelegate, WeatherProcessorDelegate {
    func lightToggled(switchedOn: Bool) {
        overlay.isHidden = switchedOn
    }
    func roomChanged(name: String?) {
        roomButton.setTitle(name, for: .normal)
    }
    
    //MARK: Weather Delegate Methods
    func updateCurrentTemp(temp: String) {
        if thermostatLabel.text != nil {
            clockLabel.textAlignment = .left
            thermostatLabel.text = temp
        } else {
            clockLabel.textAlignment = .center
        }
    }
    
    //MARK: Sun Delegate Methods
    func updateWallpaper(timePeriod: String) {
        roomImage.image = UIImage(named: timePeriod)
        loadingScreen.isHidden = true
    }
    
    //MARK: - Classes
    
    let drivers = DriverList.shared
    let localeModel = LocaleModel.shared
    let sunModel = SunModel.shared
    let weatherProcessor = WeatherProcessor.shared
    
    //MARK: - Properties and Outlets
    
    @IBOutlet weak var welcomeScreen: UIView!
    @IBOutlet weak var dismissWelcome: UIButton!
    @IBOutlet weak var tempatureLabel: UIButton!
    @IBOutlet weak var buttonBackground: UIView!
    @IBOutlet weak var roomButton: UIButton!
    @IBOutlet weak var overlay: UIView!
    @IBOutlet weak var logo: UIView!
    @IBOutlet weak var roomImage: UIImageView!
    @IBOutlet weak var clockLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var thermostatLabel: UILabel!
    @IBOutlet weak var screensaver: UIStackView!
    @IBOutlet weak var lightButtonOutlet: UIButton!
    @IBOutlet weak var screensaverDismiss: UIButton!
    @IBOutlet weak var rightSwipeBar: UIView!
    @IBOutlet weak var leftSwipeBar: UIView!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var screensaverBottom: NSLayoutConstraint!
    @IBOutlet weak var screensaverCenter: NSLayoutConstraint!
    @IBOutlet weak var logoX: NSLayoutConstraint!
    @IBOutlet weak var logoY: NSLayoutConstraint!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var insta: UIButton!
    @IBOutlet weak var loadingScreen: UIView!
    
    var timer = Timer()
    var temperatureTimer = Timer()
    var sceneCooldown = true
    var screensaverOn = false
    var SSCounter = 0
    var loadingDelay = 0
    let defaults = UserDefaults.standard
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: - Buttons
    
    @IBAction func IGPressed(_ sender: Any) {
        vibrate(intensity: .light)
        if let url = URL(string: "https://www.instagram.com/homekitsmate/"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
    }
    @IBAction func launchSupportVideos(_ sender: Any) {
        vibrate(intensity: .light)
    }
    @IBAction func button2(_ sender: Any) {
        activateLink(button: "button2")
    }
    @IBAction func button3(_ sender: Any) {
        activateLink(button: "button3")
    }
    @IBAction func button4(_ sender: Any) {
        activateLink(button: "button4")
    }
    @IBAction func amazonLinkPressed(_ sender: Any) {
        vibrate(intensity: .light)
        
        let title = "Add devices tested by mate."
        let deviceButtons : [String] = ["Philips Hue Bulbs", "Honeywell Thermostat", "Smart Lock", "MyQ Garage", "Apple TV", "Sprinkler Controller"]
        
        let optionMenu = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        optionMenu.addAction(UIAlertAction(title: "mate. merch", style: .default, handler:{ action in
            if let url = URL(string: "https://shop.spreadshirt.com/mate1/all"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }
        }))
        for device in deviceButtons.enumerated() {
            optionMenu.addAction(UIAlertAction(title: device.element, style: .default, handler:{ action in
                self.showDevice(device: action.title!, index: device.offset)
            }))
        }
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    fileprivate func chooseCountry() {
        let regions = localeModel.regions.sorted() { $0 < $1 }
        let optionMenu = UIAlertController(title: "Change Country", message: nil, preferredStyle: .actionSheet)
        for item in regions {
            optionMenu.addAction(UIAlertAction(title: item.key, style: .default, handler: changeRegion(action:)))
        }
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    @objc func longTap(_ sender: UIGestureRecognizer){
        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            
            // popover(makeMenu(tapGesture: sender))
            vibrate(intensity: .heavy)
            
            if sender.view as? UIButton == plusButton {
                chooseCountry()
            } else
            
            if let button = sender.view as? UIButton,
                let position = buttonStack.arrangedSubviews.firstIndex(where: {$0 == button}) {
                print(position)
                let categoryTitle = roomCategory(rawValue: position)?.description
                var displayTitle = categoryTitle
                
                if categoryTitle == "HomeControl" {
                    displayTitle = "Home Control"
                }
                
                let optionMenu = UIAlertController(title: displayTitle, message: nil, preferredStyle: .actionSheet)
                
                let unsortedDrivers = drivers.returnDrivers(for: .rooms).filter({$0.type.rawValue == categoryTitle})
                
                if categoryTitle == "Videos" {
                    for item in unsortedDrivers {
                        optionMenu.addAction(UIAlertAction(title: item.id, style: .default, handler:{ action in
                            self.defaults.set(action.title!, forKey: categoryTitle!)
                        }))
                    }
                } else {
                    for item in unsortedDrivers.sorted(by: {$0.id < $1.id}) {
                        optionMenu.addAction(UIAlertAction(title: item.id, style: .default, handler:{ action in
                            self.defaults.set(action.title!, forKey: categoryTitle!)
                            self.defaults.set(categoryTitle, forKey: "button\(position + 1)")
                        }))
                    }
                }
                
                optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                if let popoverController = optionMenu.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                }
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
    }
    
    func addMenu(position: Int, button: Int = 0) {
        let title = roomCategory(rawValue: position)?.description
        print(title!)
        
        let optionMenu = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        for item in drivers.returnDrivers(for: .rooms).filter({$0.type.rawValue == title}).sorted(by: {$0.id < $1.id}) {
            optionMenu.addAction(UIAlertAction(title: item.id, style: .default, handler:{ action in
                
                self.defaults.set(action.title!, forKey: title!)
                
            }))
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    func activateLink(button: String) {
        vibrate(intensity: .light)
        guard let link = defaults.string(forKey: button) else { return }
        guard let userID = defaults.string(forKey: link) else {
            print("User default not set")
            return
        }
        guard let userDriver = drivers.driver(for: userID) else {
            print("User default \(userID) did not match a driver")
            return
        }
        if let url = URL(string: userDriver.directLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: userDriver.safeLink),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
    }
    func showDevice(device: String, index: Int = 0) {
        let key = "device\(index + 1)Link"
        print(device)
        print(key)
        if let url = URL(string: defaults.string(forKey: key)!),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        } else if let url = URL(string: "https://www.homekitsmate.com/devices"),
            UIApplication.shared.canOpenURL(url) {
            print(defaults.string(forKey: "amazon")!)
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
        
    }
    func changeRegion(action: UIAlertAction) {
        guard let name = action.title else {
            return
        }
        let regionID = localeModel.regions[name]!
        let region = localeModel.updateLinkCountry(region: regionID)
        defaults.set(region["link"]!, forKey: "amazon")
        defaults.set(region["roomText"]!, forKey: "roomText")
        defaults.set(region["shadeText"]!, forKey: "shadeText")
        defaults.set(region["demoTitle"]!, forKey: "demoTitle")
        defaults.set(region["device1Link"], forKey: "device1Link")
        defaults.set(region["device2Link"], forKey: "device2Link")
        defaults.set(region["device3Link"], forKey: "device3Link")
        defaults.set(region["device4Link"], forKey: "device4Link")
        defaults.set(region["device5Link"], forKey: "device5Link")
        defaults.set(region["device6Link"], forKey: "device6Link")
    }
    func vibrate(intensity: UIImpactFeedbackGenerator.FeedbackStyle) {
        let generator = UIImpactFeedbackGenerator(style: intensity)
        generator.impactOccurred()
    }
    
    //MARK: - App Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
//        Flurry.logEvent("Show Room Page")
        roomImage.image = UIImage(named: sunModel.twoDayTimes[sunModel.currentTimePeriod].timePeriod)
        showBars()
        screensaverDismiss(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherProcessor.weatherObservers.append(self)
        if !defaults.bool(forKey: "Welcome") {
            welcomeScreen.isHidden = false
            plusButton.isHidden = true
            
        } else if defaults.bool(forKey: "Welcome") {
            welcomeScreen.isHidden = true
            plusButton.isHidden = false
        }
        sunModel.delegate = self
        roomButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        NotificationCenter.default.addObserver(self, selector: #selector(activateScreensaver), name: Notification.Name.TimeOutUserInteraction, object: nil)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
        temperatureTimer = Timer.scheduledTimer(timeInterval: 900, target: self, selector:#selector(self.weatherRefresh) , userInfo: nil, repeats: true)
        
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture4 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let longGesture5 = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        button1.addGestureRecognizer(longGesture1)
        button2.addGestureRecognizer(longGesture2)
        button3.addGestureRecognizer(longGesture3)
        button4.addGestureRecognizer(longGesture4)
        plusButton.addGestureRecognizer(longGesture5)
        print(drivers.drivers.count)
        
        sunModel.observers.append(self)
        weatherProcessor.weatherDelegate = self
        weatherProcessor.addedCells()
        AppStoreReviewManager.requestReviewIfAppropriate()
    }
    override func viewDidDisappear(_ animated: Bool) {
        screensaverDismiss(self)
    }
    
    //MARK: - Timers
    
    @objc func tick() {
        clockLabel.text = DateFormatter.localizedString(from: Date(),
                                                        dateStyle: .none,
                                                        timeStyle: .short)
        dateLabel.text = DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .none)
        
        bars += 1
        if bars >= 3 {
            hideBars()
        }
        
        loadingDelay += 1
        if loadingDelay > 5 {
            loadingScreen.isHidden = true
        }
        if screensaverOn && SSCounter >= 60 {
            SSCounter = 0
            animScreensaver()
        } else {
            SSCounter += 1
        }
        
        button2.setBackgroundImage(UIImage(named: defaults.string(forKey: "button2")!), for: .normal)
        button3.setBackgroundImage(UIImage(named: defaults.string(forKey: "button3")!), for: .normal)
        
    }
    @objc func weatherRefresh() {
        appDelegate.setupLocationManager()
    }
    
    //MARK: - Screensaver Methods
    
    @objc func activateScreensaver() {
        if let temperature = weatherProcessor.temperature {
            updateCurrentTemp(temp: temperature)
        }
        if !screensaverDisabled {
            screensaverOn = true
            buttonBackground.isHidden = true
            roomButton.isHidden = true
            screensaver.isHidden = false
            screensaverDismiss.isHidden = false
            plusButton.isHidden = true
            insta.isHidden = true
            animScreensaver()
        }
    }
    @IBAction func screensaverDismiss(_ sender: Any) {
        screensaverOn = false
        buttonBackground.isHidden = false
        roomButton.isHidden = false
        screensaver.isHidden = true
        screensaverDismiss.isHidden = true
        leftSwipeBar.isHidden = false
        rightSwipeBar.isHidden = false
        plusButton.isHidden = false
        insta.isHidden = false
    }
    func animScreensaver() {
        anim(constraintParent: self.view) { (settings) -> (animClosure) in
            settings.duration = 30
            settings.ease = .easeInOutQuad
            return {
                self.screensaverBottom.constant = 53
                self.screensaverCenter.constant = 1
                self.logoX.constant = -1
                if self.traitCollection.horizontalSizeClass == .compact {
                    self.logoY.constant = -21
                } else {
                    self.logoY.constant = -41
                }
            }
            }.then (constraintParent: self.view) { (settings) -> (animClosure) in
                settings.duration = 30
                settings.ease = .easeInOutQuad
                return {
                    self.screensaverBottom.constant = 52
                    self.screensaverCenter.constant = -1
                    self.logoX.constant = 1
                    if self.traitCollection.horizontalSizeClass == .compact {
                        self.logoY.constant = -19
                    } else {
                        self.logoY.constant = -39
                    }
                }
        }
    }
    func timeDateFormat() {
        let date = Date()
        let customFormat = "ddMMMM"
        let gbLocale = Locale(identifier: "en_GB")
        let ukFormat = DateFormatter.dateFormat(fromTemplate: customFormat, options: 0, locale: gbLocale)
        let usLocale = Locale(identifier: "en_US")
        let usFormat = DateFormatter.dateFormat(fromTemplate: customFormat, options: 0, locale: usLocale)
        let formatter = DateFormatter()
        formatter.dateFormat = ukFormat
        formatter.string(from: date)
        formatter.dateFormat = usFormat
        formatter.string(from: date)
    }
    
    //MARK: - Bars
    
    var bars = 0
    func showBars() {
        leftSwipeBar.isHidden = false
        rightSwipeBar.isHidden = false
        bars = 0
    }
    func hideBars() {
        leftSwipeBar.isHidden = true
        rightSwipeBar.isHidden = true
    }
    
    //MARK: - Welcome Screen
    
    @IBAction func dismissWelcome(_ sender: Any) {
        welcomeScreen.isHidden = true
        dismissWelcome.isHidden = true
        plusButton.isHidden = false
        defaults.set(true, forKey: "Welcome")
    }
}

enum roomCategory : Int {
    case Videos
    case Music
    case TV
    case HomeControl
    
    var description: String {
        switch self {
        case .Videos:
            return "Videos"
        case .Music:
            return "Music"
        case .TV:
            return "TV"
        case .HomeControl:
            return "HomeControl"
        }
    }
}
