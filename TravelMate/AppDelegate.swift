//
//  AppDelegate.swift
//  TravelMate
//
//  Created by Landon Brambert on 10/8/19.
//  Copyright © 2019 ONYX. All rights reserved.
//

import UIKit
import CoreLocation
//import Flurry_iOS_SDK

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    let localeModel = LocaleModel.shared
    let sunModel = SunModel.shared
    let weatherProcessor = WeatherProcessor.shared
    var locationManager: CLLocationManager?
    var currentLocation: CLLocation? {
        didSet {
            sunModel.getSunData(lat: currentLocation!.coordinate.latitude, lng: currentLocation!.coordinate.longitude)
            print("SUCCESS, current location set")
            weatherProcessor.prepareCall(lat: currentLocation!.coordinate.latitude, lng: currentLocation!.coordinate.longitude)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        Flurry.startSession("R6Z7ZZ4FZ9DVRR6P68HY", with: FlurrySessionBuilder
//            .init()
//            .withCrashReporting(true)
//            .withLogLevel(FlurryLogLevelAll))
        
        if defaults.string(forKey: "Car") == "Chevy" {
            defaults.set("Chevrolet", forKey: "Car")
        }
        
        if defaults.string(forKey: "version") == "2.0.2" {
            defaults.set("Videos", forKey: "button1")
            defaults.set("Music", forKey: "button2")
            defaults.set("TV", forKey: "button3")
            defaults.set("HomeControl", forKey: "button4")
            defaults.set("Camera", forKey: "button6")
            defaults.set("Car", forKey: "button7")
            defaults.set("Garage", forKey: "button8")
            defaults.set("Pool", forKey: "button9")
            defaults.set("Weather", forKey: "button10")
            defaults.set("Security", forKey: "button11")
            defaults.set("Thermostat", forKey: "button12")
            defaults.set("Lock", forKey: "button13")
            defaults.set("Appliances", forKey: "button14")
            defaults.set("BabyMonitors", forKey: "button15")
            defaults.set("Shade", forKey: "button16")
            defaults.set("Garage", forKey: "button8image")
            defaults.set("Pool", forKey: "button9image")
            defaults.set("Lock", forKey: "button13image")
            defaults.set("Appliances", forKey: "button14image")
            defaults.set("Somfy", forKey: "Shade")
            defaults.set("Total Connect", forKey: "Security")
            defaults.set("HomeKit", forKey: "HomeControl")
            defaults.set("Ecobee", forKey: "Thermostat")
            defaults.set("2.0.2.1", forKey: "version")
        } else if defaults.string(forKey: "version") != "2.0.3" || defaults.string(forKey: "version") == "2.0.2.1" || defaults.string(forKey: "version") != "2.1.0" {
            defaults.set("Videos", forKey: "button1")
            defaults.set("Music", forKey: "button2")
            defaults.set("TV", forKey: "button3")
            defaults.set("HomeControl", forKey: "button4")
            defaults.set("Camera", forKey: "button6")
            defaults.set("Car", forKey: "button7")
            defaults.set("Garage", forKey: "button8")
            defaults.set("Pool", forKey: "button9")
            defaults.set("Weather", forKey: "button10")
            defaults.set("Security", forKey: "button11")
            defaults.set("Thermostat", forKey: "button12")
            defaults.set("Lock", forKey: "button13")
            defaults.set("Appliances", forKey: "button14")
            defaults.set("BabyMonitors", forKey: "button15")
            defaults.set("Shade", forKey: "button16")
            defaults.set("Beginner", forKey: "Videos")
            defaults.set("Apple Music", forKey: "Music")
            defaults.set("Apple TV", forKey: "TV")
            defaults.set("HomeKit", forKey: "HomeControl")
            defaults.set("Total Connect", forKey: "Security")
            defaults.set("Honeywell Home", forKey: "Thermostat")
            defaults.set("Somfy", forKey: "Shade")
            defaults.set("Luma", forKey: "Camera")
            defaults.set("Schlage", forKey: "Lock")
            defaults.set("MyQ", forKey: "Garage")
            defaults.set("Tesla", forKey: "Car")
            defaults.set("Petzi", forKey: "Pets")
            defaults.set("iAquaLink", forKey: "Pool")
            defaults.set("AccuWeather", forKey: "Weather")
            defaults.set("SproutsIOGrow", forKey: "Plants")
            defaults.set("Hubble", forKey: "BabyMonitors")
            defaults.set("LG SmartThinQ", forKey: "Appliances")
            defaults.set("2.1.0", forKey: "version")
            defaults.set("Garage", forKey: "button8image")
            defaults.set("Pool", forKey: "button9image")
            defaults.set("Lock", forKey: "button13image")
            defaults.set("Appliances", forKey: "button14image")
        }
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["link"], forKey: "amazon")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["demoTitle"], forKey: "demoTitle")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["shadeText"], forKey: "shadeText")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["roomText"], forKey: "roomText")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["device1Link"], forKey: "device1Link")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["device2Link"], forKey: "device2Link")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["device3Link"], forKey: "device3Link")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["device4Link"], forKey: "device4Link")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["device5Link"], forKey: "device5Link")
        defaults.set(localeModel.updateLinkCountry(region: Locale.current.regionCode ?? "US")["device6Link"], forKey: "device6Link")
        
        if defaults.bool(forKey: "calibrated") != true {
            _ = Calibrator()
            defaults.set(true, forKey: "calibrated")
        }
        
        setupLocationManager()
        
        return true
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        setupLocationManager()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setupLocationManager() {
        if defaults.bool(forKey: "useDefault") == false {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            self.locationManager?.requestWhenInUseAuthorization()
            locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager?.startUpdatingLocation()
            print("Updating Location")
        } else if let home = defaults.string(forKey: "homeDefault") {
            print("home default set, using default home")
            weatherProcessor.searchWeather(search: home, sender: "appDelegate")
        } else {
            print("default home not set, using current location")
            defaults.set(false, forKey: "useDefault")
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        currentLocation = locations.last
        locationManager?.stopMonitoringSignificantLocationChanges()
//        let locationValue:CLLocationCoordinate2D = manager.location!.coordinate
//        print("locations = \(locationValue)")
        locationManager?.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        weatherProcessor.weatherDelegate?.failedToRefresh()
        print("Location mannager failed: \(error)")
    }
}

extension NSNotification.Name {
    public static let TimeOutUserInteraction: NSNotification.Name = NSNotification.Name(rawValue: "TimeOutUserInteraction")
}



class InterractionUIApplication: UIApplication {
    
    static let ApplicationDidTimoutNotification = "AppTimout"
    
    // The timeout in seconds for when to fire the idle timer.
    let timeoutInSeconds: TimeInterval = 120
    
    var idleTimer: Timer?
    
    // Listen for any touch. If the screen receives a touch, the timer is reset.
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        
        if idleTimer != nil {
            self.resetIdleTimer()
        }
        
        if let touches = event.allTouches {
            for touch in touches {
                if touch.phase == UITouch.Phase.began {
                    self.resetIdleTimer()
                }
            }
        }
    }
    
    // Resent the timer because there was user interaction.
    func resetIdleTimer() {
        if let idleTimer = idleTimer {
            idleTimer.invalidate()
        }
        
        idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds, target: self, selector: #selector(self.idleTimerExceeded), userInfo: nil, repeats: false)
    }
    
    // If the timer reaches the limit as defined in timeoutInSeconds, post this notification.
    @objc func idleTimerExceeded() {
        NotificationCenter.default.post(name:Notification.Name.TimeOutUserInteraction, object: nil)
    }


    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

