//
//  SunModel.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 5/10/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import Foundation
import SwiftDate
import HomeKit
import Alamofire
import SwiftyJSON

protocol SunModelDelegate: class {
    func updateWallpaper(timePeriod: String)
}

class SunModel {
    
    let SUN_URL = "https://api.sunrise-sunset.org/json?"
    var homeManager = HMHomeManager()
    var currentTimePeriod = 6
    static var shared = SunModel()
    
    weak var delegate: SunModelDelegate?
    
    var observers: [SunModelDelegate] = []
    
    var timePeriods : [SunObject] = [
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject()
    ]
    var yesterdayTimePeriods : [SunObject] = [
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject(),
        SunObject()
    ]
    var twoDayTimes : [SunObject] = []
    
    init() {
        adjust()
        twoDayTimes[0].timePeriod = "sunrise"
        twoDayTimes[1].timePeriod = "morning"
        twoDayTimes[2].timePeriod = "solar_noon"
        twoDayTimes[3].timePeriod = "afternoon"
        twoDayTimes[4].timePeriod = "sunset"
        twoDayTimes[5].timePeriod = "late_sunset"
        twoDayTimes[6].timePeriod = "goodnight"
        twoDayTimes[7].timePeriod = "sunrise"
        twoDayTimes[8].timePeriod = "morning"
        twoDayTimes[9].timePeriod = "solar_noon"
        twoDayTimes[10].timePeriod = "afternoon"
        twoDayTimes[11].timePeriod = "sunset"
        twoDayTimes[12].timePeriod = "late_sunset"
        twoDayTimes[13].timePeriod = "goodnight"
        twoDayTimes[0].scene = "sunrise"
        twoDayTimes[1].scene = "morning"
        twoDayTimes[2].scene = "noon"
        twoDayTimes[3].scene = "afternoon"
        twoDayTimes[4].scene = "evening"
        twoDayTimes[5].scene = "sunset"
        twoDayTimes[6].scene = "goodnight"
        twoDayTimes[7].scene = "sunrise"
        twoDayTimes[8].scene = "morning"
        twoDayTimes[9].scene = "noon"
        twoDayTimes[10].scene = "afternoon"
        twoDayTimes[11].scene = "evening"
        twoDayTimes[12].scene = "sunset"
        twoDayTimes[13].scene = "goodnight"
    }
    
    func getSunData(lat: Double, lng: Double) {
        let sunParams : [String : String] = ["lat" : "\(lat)", "lng" : "\(lng)", "formatted" : "0"]
        Alamofire.request(SUN_URL, method: .get, parameters: sunParams).responseJSON {
            response in
//            print("GETTING SUN DATA")
            if response.result.isSuccess {
                let sunJSON : JSON = JSON(response.result.value!)
                self.updateSunData(json: sunJSON)
            }
            else {
                print("Error \(String(describing: response.result.error))")
            }
        }
    }
    
    func updateSunData(json: JSON) {
        
        for (index, object) in self.timePeriods.enumerated() {
            if let test = json["results"][object.timePeriod].string?.toDate() {
                self.timePeriods[index].date = test
            }
//            let test = json["results"][object.timePeriod].string
//            print(test)
//            print(object.timePeriod)
////            self.timePeriods[index].date = test.toDate()
        }
        //triggerScene()
        findTimePeriod()
    }
    
    func findTimePeriod() {
        
        adjust()
        
//        return twoDayTimes[currentTimePeriod].timePeriod
        
        
    }
    
    func adjust() {
        
        for (index, _) in timePeriods.enumerated() {
            if timePeriods[index].timePeriod == "morning" {
                timePeriods[index].date = timePeriods[0].date + 1.hours
            } else if timePeriods[index].timePeriod == "afternoon" {
                timePeriods[index].date = timePeriods[2].date + 2.hours
            } else if timePeriods[index].timePeriod == "late_sunset" {
                timePeriods[index].date = timePeriods[4].date + 30.minutes
            } else if timePeriods[index].timePeriod == "goodnight" {
                timePeriods[index].date = timePeriods[5].date + 30.minutes
            }
        }
        
        for (index, _) in yesterdayTimePeriods.enumerated() {
            yesterdayTimePeriods[index].date = timePeriods[index].date - 1 .days
        }
        twoDayTimes = yesterdayTimePeriods
        let newArray1 = timePeriods
        for each in newArray1 {
            twoDayTimes.append(each)
        }
        
        isInRange()
    }
    
    func isInRange() {
        
        for (index, _) in twoDayTimes.enumerated() {
            
            let dateOne = twoDayTimes[index].date
            var dateTwo = twoDayTimes[numAfter(index: index)].date
            
            if index == 13 {
                dateTwo = dateTwo + 1.days
            }
            
            if DateInRegion(Date(), region: Region.current).isInRange(date: dateOne, and: dateTwo) {
                
                currentTimePeriod = index
                
//                currentTimePeriod = 0
                
                
                
//                print(dateOne.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current), dateTwo.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current))
//                print("-> \(twoDayTimes[index].timePeriod) <-")
            } else {
//                print(dateOne.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current), DateInRegion(Date(), region: Region.current).date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current), dateTwo.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current))
//                print(twoDayTimes[index].timePeriod)
            }
        }
        for observer in observers {
            observer.updateWallpaper(timePeriod: twoDayTimes[currentTimePeriod].timePeriod)
        }
        delegate?.updateWallpaper(timePeriod: twoDayTimes[currentTimePeriod].timePeriod)
//        print(delegate)
//        print(twoDayTimes[currentTimePeriod].timePeriod)
        
    }
    
    func numAfter(index: Int) -> Int {
        
        if index <= 12 {
            return index + 1
        }
        return 0
        
    }
//    var homeManager = HMHomeManager()
//    var currentTimePeriod = 6
//    var timePeriods : [SunObject] = [SunObject(), SunObject(), SunObject(), SunObject(), SunObject(), SunObject(), SunObject()]
//
//    func findTimePeriod() -> String {
//
//        adjust()
//        isInRange()
//        return timePeriods[currentTimePeriod].timePeriod
//
//    }
//
//    func adjust() {
//
//        for (index, _) in timePeriods.enumerated() {
//            if timePeriods[index].timePeriod == "morning" {
//                timePeriods[index].date = timePeriods[0].date + 1.hours
//            } else if timePeriods[index].timePeriod == "afternoon" {
//                timePeriods[index].date = timePeriods[2].date + 2.hours
//            } else if timePeriods[index].timePeriod == "late_sunset" {
//                timePeriods[index].date = timePeriods[4].date + 30.minutes
//            } else if timePeriods[index].timePeriod == "goodnight" {
//                timePeriods[index].date = timePeriods[5].date + 30.minutes
//            }
//        }
//
//    }
//
//    func isInRange() {
//
//        for (index, _) in timePeriods.enumerated() {
//
//            let dateOne = timePeriods[index].date
//            var dateTwo = timePeriods[numAfter(index: index)].date
//
//            if index == 6 {
//                dateTwo = dateTwo + 1.days
//            }
//
//            if DateInRegion(Date(), region: Region.current).isInRange(date: dateOne, and: dateTwo) {
//                currentTimePeriod = index
//                print(dateOne.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current), dateTwo.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current))
//                print("-> \(timePeriods[index].timePeriod) <-")
//            } else {
//                print(dateOne.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current), DateInRegion(Date(), region: Region.current).date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current), dateTwo.date.toFormat("YYYY-MM-dd HH:mm", locale: Locale.current))
//                print(timePeriods[index].timePeriod)
//            }
//        }
//
//    }
//
//    func numAfter(index: Int) -> Int {
//
//        if index <= 5 {
//            return index + 1
//        }
//        return 0
//
//    }
    
}
