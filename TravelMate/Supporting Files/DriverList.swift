//
//  DriverList.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 7/2/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import Foundation
import HomeKit

class DriverList {
    
    static var shared = DriverList()
    
    init() {
    }
    
    let driverCategories = [
        DriverCategory(id: .Camera, page: .accessories, image: "camera"),
        DriverCategory(id: .Pool, page: .accessories, image: "camera"),
        DriverCategory(id: .Shade, page: .accessories, image: "")
    ]
    
    let drivers: [Driver] = [
        //MARK: - Shades
        Driver(id: "Somfy", directLink: "somfymylink://", safeLink: "https://apps.apple.com/us/app/somfy-mylink/id920820854", type: .Shade, driverCategory: DriverCategory(id: .Shade, page: .keypad, image: "")),
        Driver(id: "Hunter Douglas", directLink: "com.hunterdouglas.powerview://", safeLink: "https://apps.apple.com/us/app/powerview/id1009384826", type: .Shade, driverCategory: DriverCategory(id: .Shade, page: .keypad, image: "")),
        Driver(id: "Momenta", directLink: "", safeLink: "https://apps.apple.com/us/app/momenta-shade-remote/id940790242", type: .Shade, driverCategory: DriverCategory(id: .Shade, page: .keypad, image: "")),
        Driver(id: "Qmotion Qube", directLink: "", safeLink: "https://apps.apple.com/us/app/qmotion-qube/id1106687100", type: .Shade, driverCategory: DriverCategory(id: .Shade, page: .keypad, image: "")),
        Driver(id: "Lutron", directLink: "LutronCaseta://", safeLink: "https://apps.apple.com/us/app/lutron-cas%C3%A9ta-ra2-select-app/id886753021", type: .Shade, driverCategory: DriverCategory(id: .Shade, page: .keypad, image: "")),
        
        //MARK: - Home Control
        Driver(id: "Lutron", directLink: "LutronCaseta://", safeLink: "https://apps.apple.com/us/app/lutron-cas%C3%A9ta-ra2-select-app/id886753021", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "Philips Hue", directLink: "hue2://", safeLink: "https://apps.apple.com/us/app/philips-hue/id1055281310", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "HomeKit", directLink: "com.apple.home://launch", safeLink: "https://apps.apple.com/us/app/home/id1110145103", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "Control4", directLink: "control4v2://", safeLink: "https://apps.apple.com/us/app/control4-for-os-2/id734435367", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "Savant", directLink: "", safeLink: "https://apps.apple.com/us/app/savant-pro/id1095325838", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "")),
        Driver(id: "Crestron", directLink: "crestron://", safeLink: "https://apps.apple.com/us/app/crestron/id643820704", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "RTiPanel", directLink: "rtipanel://", safeLink: "https://apps.apple.com/us/app/rtipanel/id454328684", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "Cox Homelife", directLink: "coxhomelife://", safeLink: "https://apps.apple.com/us/app/cox-homelife/id451535832", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "Total Connect", directLink: "totalconnect://", safeLink: "https://apps.apple.com/us/app/total-connect-2-0/id439763870", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "iHome", directLink: "ihomecontrol://", safeLink: "https://apps.apple.com/us/app/ihome-control/id937056509", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        Driver(id: "Wemo", directLink: "wemo://", safeLink: "https://apps.apple.com/us/app/wemo/id511376996", type: .HomeControl, driverCategory: DriverCategory(id: .HomeControl, page: .rooms, image: "HomeControl")),
        
        //MARK: - Pets
        Driver(id: "Petzi", directLink: "petzi://", safeLink: "https://apps.apple.com/us/app/petzi/id951250483", type: .Pets, driverCategory: DriverCategory(id: .Pets, page: .keypad, image: "Pets")),
        Driver(id: "Furbo", directLink: "furbo://", safeLink: "https://apps.apple.com/us/app/furbo-dog-camera/id999707163", type: .Pets, driverCategory: DriverCategory(id: .Pets, page: .keypad, image: "Pets")),
        Driver(id: "Petcube", directLink: "petcube://", safeLink: "https://apps.apple.com/us/app/petcube/id720151500", type: .Pets, driverCategory: DriverCategory(id: .Pets, page: .keypad, image: "Pets")),
        Driver(id: "PetSafe SMART DOG Trainer", directLink: "", safeLink: "https://apps.apple.com/us/app/petsafe-smart-dog-trainer/id1143643880", type: .Pets, driverCategory: DriverCategory(id: .Pets, page: .keypad, image: "Pets")),
        Driver(id: "Petnet Mobile", directLink: "petnet://", safeLink: "https://apps.apple.com/us/app/petnet-mobile/id962239016", type: .Pets, driverCategory: DriverCategory(id: .Pets, page: .keypad, image: "Pets")),
        Driver(id: "Wag", directLink: "dlwag://", safeLink: "https://apps.apple.com/us/app/wag-instant-dog-walkers/id940734609", type: .Pets, driverCategory: DriverCategory(id: .Pets, page: .keypad, image: "Pets")),
        
        //MARK: - Cameras
        Driver(id: "Ring", directLink: "ring://", safeLink: "https://apps.apple.com/us/app/ring-always-home/id926252661", type: .Camera, driverCategory: DriverCategory(id: .Camera, page: .accessories, image: "camera")),
        Driver(id: "Visualint", directLink: "", safeLink: "https://itunes.apple.com/us/app/visualint-line/id816164025?mt=8", type: .Camera, driverCategory: DriverCategory(id: .Camera, page: .accessories, image: "camera")),
        Driver(id: "Luma", directLink: "hik.intent.action.SnapAV://", safeLink: "https://apps.apple.com/us/app/luma-surveillance/id999425761", type: .Camera, driverCategory: DriverCategory(id: .Camera, page: .accessories, image: "camera")),
        Driver(id: "Alfred", directLink: "com.kalavision.alfred://", safeLink: "https://apps.apple.com/us/app/alfred-home-security-camera/id966460837", type: .Camera, driverCategory: DriverCategory(id: .Camera, page: .accessories, image: "camera")),
        Driver(id: "Avigilon Blue", directLink: "", safeLink: "https://apps.apple.com/us/app/avigilon-blue/id1228673827", type: .Camera, driverCategory: DriverCategory(id: .Camera, page: .accessories, image: "camera")),
        
        //MARK: - TV
        Driver(id: "YouTube TV", directLink: "youTubeTV://", safeLink: "https://apps.apple.com/us/app/youtube-tv/id1193350206", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Roku", directLink: "roku://", safeLink: "https://apps.apple.com/us/app/roku/id482066631", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Apple TV", directLink: "com.apple.TVRemote://launch", safeLink: "https://itunes.apple.com/us/app/apple-tv-remote/id1096834193?mt=8", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Fire TV", directLink: "FireTV://", safeLink: "https://apps.apple.com/us/app/amazon-fire-tv/id947984433", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Android TV", directLink: "", safeLink: "https://itunes.apple.com/us/app/android-tv/id1078761166?mt=8", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "DirecTV NOW", directLink: "dfw://", safeLink: "https://apps.apple.com/us/app/directv-now/id1136238277", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "TiVo", directLink: "", safeLink: "https://apps.apple.com/us/app/tivo/id401673976", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Cox Contour", directLink: "contour2://", safeLink: "https://apps.apple.com/us/app/cox-contour/id995486362", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Xfinity Stream", directLink: "xtv://", safeLink: "https://apps.apple.com/us/app/xfinity-stream/id731629156", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Spectrum TV", directLink: "spectrumTV://", safeLink: "https://apps.apple.com/us/app/spectrum-tv/id420455839", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "Xbox", directLink: "smartglass://", safeLink: "https://apps.apple.com/us/app/xbox/id736179781", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "PS4 Remote Play", directLink: "com.playstation.remoteplay.sceappcall://", safeLink: "https://apps.apple.com/us/app/ps4-remote-play/id1436192460", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        Driver(id: "PlayStation Vue", directLink: "psvue://", safeLink: "https://apps.apple.com/us/app/playstation-vue/id957987596", type: .TV, driverCategory: DriverCategory(id: .TV, page: .rooms, image: "TV")),
        
        //MARK: - Music
        Driver(id: "YouTube Music", directLink: "YouTubeMusic://", safeLink: "https://apps.apple.com/us/app/youtube-music/id1017492454", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        Driver(id: "Spotify", directLink: "Spotify://", safeLink: "https://apps.apple.com/us/app/spotify-music-and-podcasts/id324684580", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        Driver(id: "Sonos", directLink: "Sonos://", safeLink: "https://apps.apple.com/us/app/sonos-controller/id293523031", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        Driver(id: "Pandora", directLink: "Pandora://", safeLink: "https://apps.apple.com/us/app/pandora-music-podcasts/id284035177", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        Driver(id: "Deezer", directLink: "Deezer://", safeLink: "https://apps.apple.com/us/app/deezer-music-podcast-player/id292738169", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        Driver(id: "Tidal", directLink: "Tidal://", safeLink: "https://apps.apple.com/us/app/tidal-music-streaming/id913943275", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        Driver(id: "Apple Music", directLink: "music://", safeLink: "https://apps.apple.com/us/app/apple-music/id1108187390", type: .Music, driverCategory: DriverCategory(id: .Music, page: .rooms, image: "music")),
        
        //MARK: - Security
        Driver(id: "Honeywell Home", directLink: "honlyric://", safeLink: "https://apps.apple.com/us/app/honeywell-home/id880332077", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "Vivint", directLink: "vivint://", safeLink: "https://apps.apple.com/us/app/vivint-smart-home/id734547946", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "DSC", directLink: "", safeLink: "https://itunes.apple.com/us/app/habit-dsc-alarm-monitor/id448994426?mt=8", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "DMP", directLink: "virtualkeypadapp://", safeLink: "https://apps.apple.com/us/app/virtual-keypad/id447825243", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "Simplisafe", directLink: "", safeLink: "https://itunes.apple.com/us/app/simplisafe-home-security-app/id555798051?mt=8", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "ADT", directLink: "adtpulse://", safeLink: "https://apps.apple.com/us/app/adt-pulse/id355736423", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "Alarm.com", directLink: "alarmcom://", safeLink: "https://apps.apple.com/us/app/alarm-com/id315010649", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "Netatmo Security", directLink: "netatmosecurity://", safeLink: "https://apps.apple.com/us/app/netatmo-security/id951725393", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "August Home", directLink: "AugustHome://", safeLink: "https://apps.apple.com/us/app/august-home/id648730592", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "Xfinity Home", directLink: "xfinityhome://", safeLink: "https://apps.apple.com/us/app/xfinity-home/id418965252", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "Total Connect", directLink: "totalconnect://", safeLink: "https://apps.apple.com/us/app/total-connect-2-0/id439763870", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        Driver(id: "UltraSync+", directLink: "ultrasyncplus://", safeLink: "https://apps.apple.com/us/app/ultrasync/id1103085533", type: .Security, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "security")),
        
        //MARK: - Thermostats
        Driver(id: "Lennox iComfort WiFi", directLink: "", safeLink: "https://apps.apple.com/us/app/lennox-icomfort-wi-fi/id527608198", type: .Thermostat, driverCategory: DriverCategory(id: .Security, page: .rooms, image: "thermostat")),
        Driver(id: "Netatmo Energy", directLink: "netatmo-thermostat://", safeLink: "https://apps.apple.com/us/app/netatmo-energy/id730893725", type: .Thermostat, driverCategory: DriverCategory(id: .Thermostat, page: .rooms, image: "thermostat")),
        Driver(id: "Nest", directLink: "nestmobile://", safeLink: "https://apps.apple.com/us/app/nest/id464988855", type: .Thermostat, driverCategory: DriverCategory(id: .Thermostat, page: .rooms, image: "thermostat")),
        Driver(id: "Ecobee", directLink: "Ecobee://", safeLink: "https://apps.apple.com/us/app/ecobee/id916985674", type: .Thermostat, driverCategory: DriverCategory(id: .Thermostat, page: .rooms, image: "thermostat")),
        Driver(id: "Honeywell Home", directLink: "honlyric://", safeLink: "https://apps.apple.com/us/app/honeywell-home/id880332077", type: .Thermostat, driverCategory: DriverCategory(id: .Thermostat, page: .rooms, image: "thermostat")),
        
        
        
        //MARK: - Appliances
        
        Driver(id: "EZPro", directLink: "", safeLink: "https://apps.apple.com/us/app/ezpro/id878443163", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "Miele@mobile", directLink: "", safeLink: "https://apps.apple.com/us/app/miele-mobile/id930406907", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "Sub-Zero Group Owners App.", directLink: "amzn-com.subzero.group.owners.app://", safeLink: "https://apps.apple.com/us/app/sub-zero-group-owners-app/id1248991284", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "Laundry - GE Appliances", directLink: "brillion.536377517672556971506f3231672f54736e4f75://", safeLink: "https://apps.apple.com/us/app/laundry-ge-appliances/id935415794", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "Whirlpool", directLink: "com.whirlpool.wpapp://", safeLink: "https://apps.apple.com/us/app/whirlpool/id1011622189", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "Dyson Link", directLink: "", safeLink: "https://apps.apple.com/us/app/dyson-link/id993135524", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "LG SmartThinQ", directLink: "line3rdp.com.lgeha.nuts://", safeLink: "https://apps.apple.com/us/app/lg-smartthinq/id993504342", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "SmartThings", directLink: "smartthings://", safeLink: "https://apps.apple.com/us/app/smartthings/id1222822904", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "iRobot HOME", directLink: "iRobotHome://", safeLink: "https://apps.apple.com/us/app/irobot-home/id1012014442", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        Driver(id: "Kenmore Smart", directLink: "Assurelink://", safeLink: "https://apps.apple.com/us/app/kenmore-smart/id1092655640", type: .Appliances, driverCategory: DriverCategory(id: .Appliances, page: .keypad, image: "appliances")),
        
        //Mark: - NannyCams
        Driver(id: "Nest Cam", directLink: "nestmobile://", safeLink: "https://apps.apple.com/us/app/nest/id464988855", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "thermostat")),
        Driver(id: "iBabyCare", directLink: "wx13491187cda23ccf://", safeLink: "https://apps.apple.com/us/app/ibaby-care-app/id726845420", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "BabyMonitors")),
        Driver(id: "Hubble", directLink: "com.binatonetelecom.hubble://", safeLink: "https://apps.apple.com/us/app/hubble-for-motorola-monitors/id796352577", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "BabyMonitors")),
        Driver(id: "Nanit", directLink: "nanit://", safeLink: "https://apps.apple.com/us/app/nanit/id1137885597", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "BabyMonitors")),
        Driver(id: "Safety1st Baby Monitor", directLink: "dorel://", safeLink: "https://apps.apple.com/us/app/safety1st-baby-monitor/id1187131712", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "BabyMonitors")),
        Driver(id: "Baby Monitor Annie: Nanny Cam", directLink: "fb1595040400593951://", safeLink: "https://apps.apple.com/us/app/baby-monitor-annie-nanny-cam/id944269894", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "BabyMonitors")),
        Driver(id: "Owlet Care", directLink: "owlet://", safeLink: "https://apps.apple.com/us/app/new-owlet/id1343309759", type: .BabyMonitors, driverCategory: DriverCategory(id: .BabyMonitors, page: .keypad, image: "BabyMonitors")),
        
        //MARK: - Pools
        Driver(id: "iAquaLink", directLink: "", safeLink: "https://itunes.apple.com/us/app/iaqualink/id443059509?mt=8", type: .Pool, driverCategory: DriverCategory(id: .Pool, page: .accessories, image: "pool")),
        Driver(id: "Pentair", directLink: "", safeLink: "https://itunes.apple.com/us/app/screenlogic-connect/id661530011?mt=8", type: .Pool, driverCategory: DriverCategory(id: .Pool, page: .accessories, image: "pool")),
        Driver(id: "OmniLogic", directLink: "testHayward://", safeLink: "https://apps.apple.com/us/app/omnilogic/id796282158", type: .Pool, driverCategory: DriverCategory(id: .Pool, page: .accessories, image: "pool")),
        Driver(id: "Coral Drowning Detection", directLink: "", safeLink: "https://apps.apple.com/us/app/coral-drowning-detection/id1390359336", type: .Pool, driverCategory: DriverCategory(id: .Pool, page: .accessories, image: "pool")),
        //MARK: - Locks
        Driver(id: "Schlage", directLink: "schlagesense://", safeLink: "https://apps.apple.com/us/app/schlage-home/id1038212933", type: .Lock, driverCategory: DriverCategory(id: .Lock, page: .keypad, image: "lock")),
        Driver(id: "Yale", directLink: "", safeLink: "https://itunes.apple.com/us/app/yale-secure/id1261911820?mt=8", type: .Lock, driverCategory: DriverCategory(id: .Lock, page: .keypad, image: "lock")),
        Driver(id: "Kwikset", directLink: "", safeLink: "https://itunes.apple.com/us/app/kwikset-premis-touchscreen-smart-lock/id1097161999?mt=8", type: .Lock, driverCategory: DriverCategory(id: .Lock, page: .keypad, image: "lock")),
        Driver(id: "Kevo", directLink: "kevo://", safeLink: "https://apps.apple.com/us/app/kevo/id685604951", type: .Lock, driverCategory: DriverCategory(id: .Lock, page: .keypad, image: "lock")),
        //MARK: - Garage Doors
        Driver(id: "MyQ", directLink: "myliftmaster://", safeLink: "https://apps.apple.com/us/app/myq-garage-access-control/id456282559", type: .Garage, driverCategory: DriverCategory(id: .Garage, page: .accessories, image: "garage")),
        Driver(id: "Smart Gate", directLink: "", safeLink: "https://apps.apple.com/us/app/smart-gate/id1440418448", type: .Garage, driverCategory: DriverCategory(id: .Garage, page: .accessories, image: "garage")),
        //MARK: - Cars
        Driver(id: "Tesla", directLink: "teslamotors://", safeLink: "https://apps.apple.com/us/app/tesla/id582007913", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Lincoln", directLink: "lincolnapp://", safeLink: "https://apps.apple.com/us/app/the-lincoln-way/id1141482401", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Ford", directLink: "fordapp://", safeLink: "https://apps.apple.com/us/app/fordpass-fuel-park-dealers/id1095418609", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Chevrolet", directLink: "mychevrolet-owner://", safeLink: "https://apps.apple.com/us/app/mychevrolet/id398596699", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Jaguar", directLink: "", safeLink: "https://itunes.apple.com/us/app/jaguar-incontrol-remote/id950640882?mt=8", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Lexus", directLink: "", safeLink: "https://itunes.apple.com/us/app/lexus-enform-remote/id863424094?mt=8", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Toyota", directLink: "", safeLink: "https://itunes.apple.com/us/app/toyota-remote-connect/id1210852946?mt=8", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Nissan", directLink: "", safeLink: "https://itunes.apple.com/us/app/nissanconnect-services/id985318403?mt=8", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Land Rover", directLink: "", safeLink: "https://itunes.apple.com/us/app/land-rover-incontrol-remote/id713993146?mt=8", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Jeep", directLink: "fcauconnect://", safeLink: "https://apps.apple.com/us/app/uconnect/id1229236724", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Chrysler", directLink: "fcauconnect://", safeLink: "https://apps.apple.com/us/app/uconnect/id1229236724", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Dodge", directLink: "fcauconnect://", safeLink: "https://apps.apple.com/us/app/uconnect/id1229236724", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Fiat", directLink: "fcauconnect://", safeLink: "https://apps.apple.com/us/app/uconnect/id1229236724", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Subaru", directLink: "mysubaru://", safeLink: "https://apps.apple.com/us/app/mysubaru/id1005186680", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Volvo", directLink: "volvooncall://", safeLink: "https://apps.apple.com/us/app/volvo-on-call/id439635293", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Honda", directLink: "com.honda.hondalink.connect://", safeLink: "https://apps.apple.com/us/app/hondalink/id750465030", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Audi", directLink: "myaudi://", safeLink: "https://apps.apple.com/us/app/myaudi/id440464115", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Mercedes", directLink: "mercedesmeapp://", safeLink: "https://apps.apple.com/us/app/mercedes-me-connect-usa/id1397873833", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Lyft", directLink: "lyft://", safeLink: "https://apps.apple.com/us/app/lyft/id529379082", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Uber", directLink: "uber://", safeLink: "https://apps.apple.com/us/app/uber/id368677368", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Cadillac", directLink: "mycadillac-owner://", safeLink: "https://apps.apple.com/us/app/mycadillac/id398605251", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "GMC", directLink: "mygmc-owner://", safeLink: "https://apps.apple.com/us/app/mygmc/id399408958", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Turo", directLink: "turo://", safeLink: "https://apps.apple.com/us/app/turo-better-than-car-rental/id555063314", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Volkswagon", directLink: "car-net://", safeLink: "https://apps.apple.com/us/app/vw-car-net-security-service/id637568471", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "BMW", directLink: "", safeLink: "https://apps.apple.com/us/app/bmw-connected/id1087277146", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        Driver(id: "Automatic CCA", directLink: "comautomaticskyline://", safeLink: "https://apps.apple.com/us/app/automatic-cca/id1426675567", type: .Car, driverCategory: DriverCategory(id: .Car, page: .accessories, image: "car")),
        
        //MARK: - Weather
        Driver(id: "AccuWeather", directLink: "AccuWeatherApp://", safeLink: "https://apps.apple.com/us/app/accuweather-weather-radar/id300048137", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "Netatmo Weather", directLink: "netatmo://", safeLink: "https://apps.apple.com/us/app/netatmo-weather/id532538499", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "PWS Monitor", directLink: "", safeLink: "https://apps.apple.com/us/app/pws-monitor/id713705929", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "Meteo Monitor for PWS", directLink: "", safeLink: "https://apps.apple.com/us/app/meteo-monitor-for-pws/id1452075868", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "La Crosse View", directLink: "", safeLink: "https://apps.apple.com/us/app/la-crosse-view/id1006925791", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "Weather - The Weather Channel", directLink: "twcweather://", safeLink: "https://apps.apple.com/us/app/weather-the-weather-channel/id295646461", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "NOAA Weather Radar Live", directLink: "noaaradarpro://", safeLink: "https://apps.apple.com/us/app/noaa-weather-radar-live/id749133753", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        Driver(id: "Weather Up", directLink: "weatherup://", safeLink: "https://apps.apple.com/us/app/weather-up/id1196015787", type: .Weather, driverCategory: DriverCategory(id: .Weather, page: .accessories, image: "weather")),
        
        //MARK: - Plants
        Driver(id: "SproutsIOGrow", directLink: "io.sprouts.sproutsiogrow://", safeLink: "https://apps.apple.com/us/app/sproutsiogrow/id1371009055", type: .Plants, driverCategory: DriverCategory(id: .Plants, page: .keypad, image: "plants")),
        Driver(id: "Rain Bird", directLink: "", safeLink: "https://apps.apple.com/us/app/rain-bird/id1060727082", type: .Plants, driverCategory: DriverCategory(id: .Plants, page: .keypad, image: "plants")),
        Driver(id: "Cloudponics", directLink: "", safeLink: "https://apps.apple.com/us/app/cloudponics/id1187343529", type: .Plants, driverCategory: DriverCategory(id: .Plants, page: .keypad, image: "plants")),
        Driver(id: "Rachio", directLink: "rachio://", safeLink: "https://apps.apple.com/us/app/rachio/id864325098", type: .Plants, driverCategory: DriverCategory(id: .Plants, page: .keypad, image: "plants")),
        
        //MARK: - Videos
        Driver(id: "Beginner", directLink: "https://www.youtube.com/playlist?list=PLTdja47JgoA4-dWlqMvvvmr9K0V9Dq0Ys", safeLink: "", type: .Videos, driverCategory: DriverCategory(id: .Videos, page: .rooms, image: "videos")),
        Driver(id: "Intermediate", directLink: "https://www.youtube.com/playlist?list=PLTdja47JgoA7r_fmfenHO2GYxYfFJGNy9", safeLink: "", type: .Videos, driverCategory: DriverCategory(id: .Videos, page: .rooms, image: "videos")),
        Driver(id: "Advanced", directLink: "https://www.youtube.com/playlist?list=PLTdja47JgoA6V01vhqMj-LIBnVvrEzciM", safeLink: "", type: .Videos, driverCategory: DriverCategory(id: .Videos, page: .rooms, image: "videos"))
    ]
    
    func directDrivers() -> [Driver] {
        var directDrivers: [Driver] = []
        for driver in drivers {
            if driver.directLink != "" {
                directDrivers.append(driver)
            }
        }
        return directDrivers
    }
    
    func canOpen(_ driver: Driver) -> Bool {
        if let url = URL(string: driver.directLink) {
            return UIApplication.shared.canOpenURL(url)
        } else {
            return false
        }
    }
    
    func returnDrivers(by driverType: String) -> [Driver] {
        return drivers.filter({ $0.type.rawValue == driverType })
    }
    
    func returnDrivers(for page: Page) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.page == page})
    }
    
    func driver(for key: String) -> Driver? {
        return drivers.first(where: { $0.id == key })
    }
    
    func returnDrivers(for category: DriverCategory) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.image == category.image })
    }
    
}

struct Calibrator {
    private let drivers = DriverList()
    init() {
        for driver in drivers.directDrivers() {
            if self.drivers.canOpen(driver) {
                print("Setting default: \(driver.id) for category: \(driver.type.rawValue)")
                defaults.set(driver.id, forKey: driver.type.rawValue)
            }
        }
    }
}
