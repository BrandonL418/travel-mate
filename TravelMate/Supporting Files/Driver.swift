//
//  Driver.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 7/2/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import UIKit

struct Driver {
    let id: String
    let directLink: String
    let safeLink: String
    let type: DriverType
    var driverCategory: DriverCategory? //{
//        return test.first(where: { $0.id == type })
//    }
    var image: UIImage {
        guard let cat = driverCategory else {
            print("DRIVER OBJECT ERROR")
            return defaultImage
        }
        
        return UIImage(named: cat.image) ?? defaultImage
    }
    
    private let defaultImage = UIImage(named: imageType.camera.rawValue) ?? UIImage()
}

//extension Driver {
//    init (direct: String, appStore: String, type: imageType, id: String = "broke") {
//        directLink = direct
//        safeLink = appStore
//        self.type = type
//        self.id = id
//    }
//}

enum DriverType: String {
    case Camera
    case Lock
    case Pets
    case Garage
    case Car
    case Music
    case TV
    case Security
    case Thermostat
    case Pool
    case Shade
    case Videos
    case Weather
    case Plants
    case Appliances
    case BabyMonitors
    case HomeControl
}

struct DriverCategory {
    let id: DriverType
    let page: Page
    let image: String
}

enum Page {
    case accessories
    case rooms
    case keypad
}


enum imageType: String {
    case camera
    case lock
    case paws
    case garage
    case car
    case music
    case TV
    case security
    case pool
    case shade
    case weather
    case plants
    case none
}
