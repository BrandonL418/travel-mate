//
//  LocaleModel.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 7/24/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import Foundation

class LocaleModel {
    
    static var shared = LocaleModel()
    
    let LINK_PLACEHOLDER = ""
    
    let regions: [String : String] = [
        "United States" : "US",
        "United Kingdom" : "GB",
        "Deutschland" : "DE",
        "France" : "FR",
//        "Japan" : "JP",
        "Canada" : "CA",
//        "China" : "CN",
        "Italia" : "IT",
        "España" : "ES",
//        "India" : "IN",
//        "Brazil" : "BR",
//        "Mexico" : "MX",
//        "Australia" : "AU",
//        "United Arab Emirates" : "AE"
    ]
    
    func updateLinkCountry(region: String) -> [String : String] {
        
        switch (region) {
            
        case "US" :
            return [
                "link" : "https://www.homekitsmate.com/devices",
//                "link" : "https://www.amazon.com/Lutron-L-BDG-WH-Caseta-Wireless-Bridge/dp/B00L5NUT16/ref=as_sl_pc_as_ss_li_til?tag=mate061-20&linkCode=w00&linkId=9234a2d2f8ff0ced26934f3b35c262bf&creativeASIN=B00L5NUT16",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://amzn.to/2zxfdRq",
                "device2Link" : "https://amzn.to/2Ueu3Wi",
                "device3Link" : "https://amzn.to/2L3bNMI",
                "device4Link" : "https://amzn.to/34ej0kS",
                "device5Link" : "https://www.amazon.com/dp/B075NHCSS4/ref=fs_a_atv_0",
                "device6Link" : "https://amzn.to/2Uf3aBG"
            ]
            
        case "GB" :
            return [
                "link" : "http://homekitsmate.com/homekit-devices-of-uk",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://amzn.to/2MHXiPL",
                "device2Link" : "https://www.amazon.co.uk/Honeywell-Home-Thermostat-HomeKit-Amazon/dp/B01M9ATDY7/ref=as_sl_pc_as_ss_li_til?tag=mate01-21&linkCode=w00&linkId=f4a84d7180a4816b685f23a8c3cea939&creativeASIN=B01M9ATDY7",
                "device3Link" : "https://www.amazon.co.uk/Schlage-Company-BE469NXCAM619-Touchscreen-Deadbolt/dp/B00AGK9KOG/ref=as_sl_pc_as_ss_li_til?tag=mate01-21&linkCode=w00&linkId=86b5aec4d83c807d1a091ae2549d5293&creativeASIN=B00AGK9KOG",
                "device4Link" : "https://www.amazon.co.uk/Chamberlain-Hub-MYQ-G0301-Upgrade-Existing/dp/B075H7Z5L8/ref=as_sl_pc_as_ss_li_til?tag=mate01-21&linkCode=w00&linkId=e777083d189541f910ded5937a8a38d8&creativeASIN=B075H7Z5L8",
                "device5Link" : "https://www.amazon.co.uk/Apple-MP7P2LL-A-TV-64GB/dp/B075NHCSS4/ref=as_sl_pc_as_ss_li_til?tag=mate01-21&linkCode=w00&linkId=24e37510733d23310473dc9a81bafaeb&creativeASIN=B075NHCSS4",
                "device6Link" : "https://www.amazon.co.uk/Rachio-Smart-Sprinkler-Controller-16-Zone/dp/B07CZ5K355/ref=as_sl_pc_as_ss_li_til?tag=mate01-21&linkCode=w00&linkId=def0e976be22e71cd70f3518c7109ddb&creativeASIN=B07CZ5K355"
            ]
            
        case "DE" :
            return [
                "link" : "https://www.homekitsmate.com/homekit-devices-germany/",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://www.amazon.de/Philips-Hue-Erweiterung-warmwei%C3%9Fes-kompatibel/dp/B0152WXI0E/ref=sr_1_17?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=homekit+light&qid=1563462202&s=gateway&sr=8-17",
                "device2Link" : "https://www.amazon.de/Honeywell-Y6R910WF6042-Raumthermostat-verdrahtete-Empf%C3%A4ngerbox/dp/B01M3PQJVV/ref=as_sl_pc_as_ss_li_til?tag=mate084-21&linkCode=w00&linkId=1fb23519e7abd547c0e414abc1a7f785&creativeASIN=B01M3PQJVV",
                "device3Link" : "https://www.amazon.de/Schlage-Connect-Touchscreen-Deadbolt-Lock/dp/B00AGK9KOG/ref=as_sl_pc_as_ss_li_til?tag=mate084-21&linkCode=w00&linkId=591370699126252f25905f2d3db9d601&creativeASIN=B00AGK9KOG",
                "device4Link" : "hbhbhgbvghyhttps://www.amazon.de/Chamberlain-Universal-Smartphone-Control-MYQ-G0301/dp/B0748KKJH6/ref=as_sl_pc_as_ss_li_til?tag=mate084-21&linkCode=w00&linkId=e632c5069df8636a2d5fbe0b7b43aa90&creativeASIN=B0748KKJH6ujes",
                "device5Link" : "https://www.amazon.de/Apple-MP7P2FD-A-TV-64GB/dp/B075LY38K7/ref=as_sl_pc_as_ss_li_til?tag=mate084-21&linkCode=w00&linkId=4b8fed15ae35c3bac4790945f788c863&creativeASIN=B075LY38K7",
                "device6Link" : "https://www.amazon.de/Rachio-Sprinkler-Controller-16-Zone-Generation/dp/B01D1NMLJU/ref=as_sl_pc_as_ss_li_til?tag=mate084-21&linkCode=w00&linkId=e3b7d41554e799a735e682043efd0ee2&creativeASIN=B01D1NMLJU"
            ]
            
        case "FR" :
            return [
                "link" : "http://homekitsmate.com/homekit-devices-of-france",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://amzn.to/2yLL409",
                "device2Link" : "https://www.amazon.fr/Honeywell-Home-Thermostat-Programmable-Connectable/dp/B01M9ATDY7/ref=as_sl_pc_as_ss_li_til?tag=mate011-21&linkCode=w00&linkId=c4b68897236add1711090f068b71c8df&creativeASIN=B01M9ATDY7",
                "device3Link" : "https://www.amazon.fr/Igloohome-IGB3-Smart-Deadbolt-Noir/dp/B07D2HQW3K/ref=as_sl_pc_as_ss_li_til?tag=mate011-21&linkCode=w00&linkId=afaa23c3f51eb38a26263396aca7cd51&creativeASIN=B07D2HQW3K",
                "device4Link" : "https://www.amazon.fr/Chamberlain-Smart-Garage-Opener-MYQ-G0301/dp/B075H7Z5L8/ref=as_sl_pc_as_ss_li_til?tag=mate011-21&linkCode=w00&linkId=a48cf8a3a8fa086fd77dc922eccbfeca&creativeASIN=B075H7Z5L8",
                "device5Link" : "https://www.amazon.fr/Apple-MP7P2FD-A-Apple-TV-4K-64Go/dp/B075LY38K7/ref=as_sl_pc_as_ss_li_til?tag=mate011-21&linkCode=w00&linkId=31a54a4ab982ab17e89c4cebc94991a3&creativeASIN=B075LY38K7",
                "device6Link" : "https://www.amazon.fr/Rachio-contr%C3%B4leur-Arroseur-Generation-5-50X1-50X9-25/dp/B01D1NMLJU/ref=as_sl_pc_as_ss_li_til?tag=mate011-21&linkCode=w00&linkId=f933df08a749f8651eda2b6dda14acd5&creativeASIN=B01D1NMLJU"
            ]
            
//        case "JP" :
//            return [
//                "link" : "https://www.amazon.com/stores/page/E713F828-942A-4FF2-A34D-F07ED233D189?ingress=0&visitId=9b1b77f2-3faf-4c34-85d1-ca0eff792042",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            ]
            
        case "CA" :
            return [
                "link" : "https://www.homekitsmate.com/homekit-devices-of-canada",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://amzn.to/2YS0TRI",
                "device2Link" : "https://www.amazon.ca/Honeywell-RCHT8612WF2015-Smart-Thermostat-Black/dp/B07FSHXV56/ref=as_sl_pc_as_ss_li_til?tag=mate08-20&linkCode=w00&linkId=eaddb6d2fa2ce5be78cf307a202de442&creativeASIN=B07FSHXV56",
                "device3Link" : "https://www.amazon.ca/Schlage-BE469ZP-CEN-619-Deadbolt/dp/B07KQRNJPL/ref=as_sl_pc_as_ss_li_til?tag=mate08-20&linkCode=w00&linkId=a9d8a4175c662a8b91cc6df1a050ade3&creativeASIN=B07KQRNJPL",
                "device4Link" : "https://amzn.to/31nfKkL",
                "device5Link" : "https://www.apple.com/ca/shop/buy-tv/apple-tv-4k/64gb",
                "device6Link" : "https://www.amazon.ca/Rachio-8ZULW-B-CAN-Sprinkler-Controller-Generation/dp/B01D1NMLJU/ref=as_sl_pc_as_ss_li_til?tag=mate08-20&linkCode=w00&linkId=76bfde759db5ed58fee0b83b64a5ebff&creativeASIN=B01D1NMLJU"
            ]
            
//        case "CN" :
//            return [
//                "link" : "Made in China",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            "device1Link" : "qwertyujkjnhbgvfd",
//            "device2Link" : "qwertyujklkmjnhbg",
//            "device3Link" : "hnbgfghjkmnjhges",
//            "device4Link" : "hbhbhgbvghyujes",
//            "device5Link" : "httwsdfghces",
//            "device6Link" : "hedfgbhnmces"
//            ]
            
        case "IT" :
            return [
                "link" : "https://www.homekitsmate.com/homekit-devices-of-italy",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://www.amazon.it/Philips-Starter-Lampadine-Connesse-Incluse/dp/B016H0R7SE/ref=as_sl_pc_as_ss_li_til?tag=mate0d3-21&linkCode=w00&linkId=5c43a096a6e966172300f10a41650ed2&creativeASIN=B016H0R7SE",
                "device2Link" : "https://www.amazon.it/Honeywell-Termostato-Programmabile-Intelligente-Wi-Fi/dp/B01M3PQJVV/ref=as_sl_pc_as_ss_li_til?tag=mate0d3-21&linkCode=w00&linkId=78dbf66ccb2e27d6be0cf4db71da0d77&creativeASIN=B01M3PQJVV",
                "device3Link" : "https://www.amazon.it/Shani-Serratura-riconoscimento-biometrico-Bluetooth/dp/B07TZYDNK4/ref=as_sl_pc_as_ss_li_til?tag=mate0d3-21&linkCode=w00&linkId=072300c637452ec610263e504fc8e569&creativeASIN=B07TZYDNK4",
                "device4Link" : "https://www.amazon.it/Chamberlain-Smart-Garage-Opener-MYQ-G0301/dp/B075H7Z5L8/ref=as_sl_pc_as_ss_li_til?tag=mate0d3-21&linkCode=w00&linkId=29b93a4622beb12150811d664cb9488e&creativeASIN=B075H7Z5L8",
                "device5Link" : "https://www.amazon.it/Apple-MP7P2QM-A-TV-64GB/dp/B075RBFS1K/ref=as_sl_pc_as_ss_li_til?tag=mate0d3-21&linkCode=w00&linkId=d225c1849a096d0371438c676ec429f6&creativeASIN=B075RBFS1K",
                "device6Link" : "https://www.amazon.it/Netro-Controller-consapevoli-Adattatore-Alimentazione/dp/B07DGS7WK9/ref=as_sl_pc_as_ss_li_til?tag=mate0d3-21&linkCode=w00&linkId=c19ba3fbcab1ad8804e1b89ef8c79932&creativeASIN=B07DGS7WK9"
            ]
            
        case "ES" :
            return [
                "link" : "https://www.homekitsmate.com/homekit-devices-of-spain",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://www.amazon.es/Philips-Hue-White-iluminaci%C3%B3n-inteligente/dp/B016H0R7SE/ref=as_sl_pc_as_ss_li_til?tag=mate07-21&linkCode=w00&linkId=76334e9d4af9ddd6e12b0a4d3b33ad66&creativeASIN=B016H0R7SE",
                "device2Link" : "https://www.amazon.es/Honeywell-Home-Termostato-programable-Inteligente/dp/B01M3PQJVV/ref=as_sl_pc_as_ss_li_til?tag=mate07-21&linkCode=w00&linkId=03b4ef8cbac14a05ac59395b1316814b&creativeASIN=B01M3PQJVV",
                "device3Link" : "https://www.amazon.es/Schlage-BE469NXCAM619-Touchscreen-Lock-Company/dp/B00AGK9KOG/ref=as_sl_pc_as_ss_li_til?tag=mate07-21&linkCode=w00&linkId=2f89f273adb41000a4d019452aa284b2&creativeASIN=B00AGK9KOG",
                "device4Link" : "https://amzn.to/2MSaGAP",
                "device5Link" : "https://amzn.to/2MT6H7j",
                "device6Link" : "https://www.amazon.es/Rachio-8ZULW-C-Zone-Controlador-Inteligente/dp/B07CZ5K355/ref=as_sl_pc_as_ss_li_til?tag=mate07-21&linkCode=w00&linkId=586def617a27049b4055c89befbc8fa4&creativeASIN=B07CZ5K355"
            ]
            
//        case "IN" :
//            return [
//                "link" : "T-Series",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            ]
//
//        case "BR" :
//            return [
//                "link" : "Yeah, it's a big festival",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            ]
//
//        case "MX" :
//            return [
//                "link" : "Inventors of the taco",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            ]
//
//        case "AU" :
//            return [
//                "link" : "Watch out for those spiders",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            ]
//
//        case "AE" :
//            return [
//                "link" : "The place with Dubai",
//                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
//                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
//                "demoTitle" : "(Demo Mode)"
//            ]
            
        default :
            return [
                "link" : "https://www.homekitsmate.com/devices",
                "roomText" : "Watch mate videos for instructions on adding scenes and lighting to HomeKit. Once finished, tap here to toggle lights.",
                "shadeText" : "Watch mate videos for instructions on adding scenes and shades to HomeKit.",
                "demoTitle" : "(Demo Mode)",
                "device1Link" : "https://amzn.to/2yJHXpF",
                "device2Link" : "https://www.amazon.com/Honeywell-Lyric-Wi-Fi-Smart-Thermostat/dp/B07DS2KZSF/ref=as_sl_pc_as_ss_li_til?tag=mate061-20&linkCode=w00&linkId=e17113f482960ea2ca0d60a7b5fd85c5&creativeASIN=B07DS2KZSF",
                "device3Link" : "https://www.amazon.com/Schlage-Connect-Deadbolt-Century-Certified/dp/B07H3XQWF7/ref=as_sl_pc_as_ss_li_til?tag=mate061-20&linkCode=w00&linkId=460dcb9c070f9a35d2eb6b20f693f046&creativeASIN=B07H3XQWF7",
                "device4Link" : "https://amzn.to/2Kju3B7",
                "device5Link" : "https://www.amazon.com/Apple-TV-64GB-Latest-Model/dp/B075NHCSS4/ref=sr_1_1?keywords=apple+tv+apple+tv+64gb&qid=1558112145&s=gateway&sr=8-1",
                "device6Link" : "https://www.amazon.com/Rachio-8ZULW-C-Sprinkler-Controller-Works/dp/B07CZ864Y9/ref=as_sl_pc_as_ss_li_til?tag=mate061-20&linkCode=w00&linkId=e232e2a601748e3b2a82a2855a82ddf5&creativeASIN=B07CZ864Y9"
            ]
        }
    }
}
