import UIKit
import HomeKit

class AccesoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var roomName: UILabel!
    var room: HMRoom? {
        didSet {
            roomName.text = room?.name
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 350, height: 350)
    }
}
