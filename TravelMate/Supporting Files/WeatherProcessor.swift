//
//  WeatherProcessor.swift
//  DateTests
//
//  Created by Brandon Lambert on 8/14/19.
//  Copyright © 2019 Brandon Lambert. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON

protocol WeatherProcessorDelegate: class {
    func updateForecastUI()
    func updateCurrentTemp(temp: String)
    func updateCurrentCity(city: String)
    func chooseResult(json: JSON, isHome: Bool)
    func failedToRefresh()
}
extension WeatherProcessorDelegate {
    //implimentation of methods listed here is not required
    func updateForecastUI() {}
    func updateCurrentCity(city: String) {}
    func chooseResult(json: JSON, isHome: Bool) {}
    func failedToRefresh() {}
}

class WeatherProcessor: CLLocationManager, CLLocationManagerDelegate {
    
    weak var weatherDelegate: WeatherProcessorDelegate?
    
    var weatherObservers: [WeatherProcessorDelegate] = []
    
    static var shared = WeatherProcessor()
    
    var weatherCells: [ForecastCell] = []
    var dayparts: [String] = []
    var temps: [String] = []
    var lowTemps: [String] = []
    var rain: [String] = []
    var imageID: [String] = []
    var refresh = Timer()
    var sunset = ""
    var forecastJSON: JSON?
    let sunModel = SunModel.shared
    var broken = false {
        didSet {
            if broken {
                print("Weather is broken")
            } else {
                print("Weather is not broken")
            }
        }
    }
    var city: String? {
        didSet {
            weatherDelegate?.updateCurrentCity(city: city!)
        }
    }
    var temperature: String? {
        didSet {
            weatherDelegate?.updateCurrentTemp(temp: temperature!)
            for observer in weatherObservers {
                observer.updateCurrentTemp(temp: temperature!)
            }
        }
    }
    var UV: Double? //{
//        didSet {
//            weatherDelegate?.updateUV(UV: UV!)
//        }
//    }
    let SEARCH_URL = "https://api.weather.com/v3/location/search"
    let LOCATION_URL = "https://api.weather.com/v3/location/near"
    let CURRENT_WEATHER_URL = "https://api.weather.com/v2/pws/observations/current?"
    let FIVE_DAY_WEATHER_URL = "https://api.weather.com/v3/wx/forecast/daily/5day"
    let APP_ID = "0e2be927e1144745abe927e114d74555"
    let locationManager = CLLocationManager()
    
    
    func prepareCall(lat: Double, lng: Double) {
        print("Preparing API call: \nLat: \(lat)\nLng: \(lng)")
        let geocode = "\(lat),\(lng)"
        print("Geocode: ", geocode)
        let params : [String : String] = ["geocode" : geocode, "product" : "pws", "format" : "json", "apiKey" : APP_ID]
        let group = DispatchGroup()
        group.enter()
        callAPI(url: LOCATION_URL, parameters: params)
        let otherParams : [String : String] = ["geocode" : geocode, "units" : "e", "language" : "en-US", "format" : "json", "apiKey" : APP_ID]
        callAPI(url: FIVE_DAY_WEATHER_URL, parameters: otherParams)
    }
    
    func callAPI(url: String, parameters: [String: String]) {
        
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON {
            response in
            print("Request: ", response.request)
            if response.result.isSuccess {
                let weatherJSON : JSON = JSON(response.result.value!)
                print(weatherJSON)
                if url == self.LOCATION_URL {
                    self.getStation(json: weatherJSON)
                } else if url == self.FIVE_DAY_WEATHER_URL {
                    print("DEBUG")
                    self.forecastJSON = weatherJSON
                    self.updateTempData(json: self.forecastJSON!)
                } else {
                    self.updateWeatherData(json: weatherJSON)
                }
                self.broken = false
            }
            else {
                print("Error \(String(describing: response.result.error))")
                self.failed()
            }
        }
    }
    
    func searchWeather(search: String, sender: String) {
        
        let parameters = ["query" : search, "language" : Locale.preferredLanguages[0], "format" : "json", "apiKey" : APP_ID]
        Alamofire.request(SEARCH_URL, method: .get, parameters: parameters).responseJSON {
            response in
            if response.result.isSuccess {
                if sender == "homeSearchBar" {
//                    print("searchBar searched")
                    self.weatherDelegate?.chooseResult(json: JSON(response.result.value!), isHome: true)
                } else if sender == "searchBar" {
//                    print("searchBar searched")
                    self.weatherDelegate?.chooseResult(json: JSON(response.result.value!), isHome: false)
                } else {
                    self.prepareCall(lat: JSON(response.result.value!)["location"]["latitude"][0].double!, lng: JSON(response.result.value!)["location"]["longitude"][0].double!)
                    self.sunModel.getSunData(lat: JSON(response.result.value!)["location"]["latitude"][0].double!, lng: JSON(response.result.value!)["location"]["longitude"][0].double!)
                }
            }
            else {
                print("Error \(String(describing: response.result.error))")
                self.failed()
            }
        }
    }
    
    
    
    func updateTempData(json : JSON) {
//         print("JSON: \(json)")
        
        dayparts = []
        rain = []
        temps = []
        lowTemps = []
        imageID = []
        
        for (index, cell) in weatherCells.enumerated() {
            if let name = json["daypart"][0]["daypartName"][index + 1].string {
//                print(name)
                //cell.dayPartNameLabel.text = name
                dayparts.append(name)
            } else {
                cell.dayPartNameLabel.text = "Today"
            }
            if let min = json["daypart"][0]["precipChance"][index + 1].int {
//                print(min)
                // cell.precipitationLabel.text = "\(min)%"
                rain.append("\(min)%")
                
            } else {
                cell.precipitationLabel.text = ""
            }
            if let max = json["temperatureMax"][index + 1].int {
//                print(max)
                //                cell.temperatureLabel.text = "\(max)º"
                temps.append("\(max)º")
            } else {
                cell.temperatureLabel.text = ""
            }
            if let min = json["temperatureMin"][index + 1].int {
//                print(min)
                //                cell.temperatureLabel.text = "\(max)º"
                lowTemps.append("\(min)º")
            } else {
                cell.temperatureLabel.text = "1000"
            }
            if let code = json["daypart"][0]["iconCode"][index + 1].int {
//                print(code)
                
                imageID.append("\(code)")
                //                cell.image.image = UIImage(named: "\(code)")
            } else {
                cell.image.image = UIImage(named: "33")
            }
            if let time = json["sunsetTimeLocal"][0].string {
                sunset = time
            }
        }
//        print("Will update forecast UI")
        weatherDelegate?.updateForecastUI()
        
    }
    
    func getStation(json: JSON) {
        let stationID = json["location"]["stationId"].arrayValue
        
        if let ID: String = stationID[safe: 0]?.string {
//            print(ID)
            let url = "\(CURRENT_WEATHER_URL)stationId=\(ID)"
            let params : [String : String] = ["format" : "json", "units" : "e", "apiKey" : "\(APP_ID)"]
            callAPI(url: url, parameters: params)
        } else {
            print("Failed to obtain PWS-ID")
        }
    }
    
    func updateWeatherData(json: JSON) {
        if let temp = json["observations"][0]["imperial"]["temp"].int {
//            print("TEMP: \(temp)")
            temperature = "\(temp)º"
        }
        if let cityName = json["observations"][0]["neighborhood"].string {
//            print("CITY: \(cityName)")
            city = cityName
        } else {
            city = "Weather"
        }
    }
    
    func addedCells() {
        if let json = forecastJSON {
//            print("lies")
            self.updateTempData(json: json)
        } else {
//            print("OOOOOF")
//            failed()
        }
        weatherDelegate?.updateCurrentCity(city: city ?? "")
        weatherDelegate?.updateCurrentTemp(temp: temperature ?? "")
//        weatherDelegate?.updateUV(UV: UV ?? 0.0)
    }
    
    func failed() {
        weatherDelegate?.failedToRefresh()
        for observer in weatherObservers {
            observer.failedToRefresh()
        }
        broken = true
    }
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
