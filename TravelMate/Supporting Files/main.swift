//
//  main.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 3/29/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import UIKit

UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    NSStringFromClass(InterractionUIApplication.self),
    NSStringFromClass(AppDelegate.self)
)

