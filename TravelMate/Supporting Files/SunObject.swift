//
//  SunObject.swift
//  OnyxRemote
//
//  Created by Brandon Lambert on 5/13/19.
//  Copyright © 2019 Trae Robbins. All rights reserved.
//

import Foundation
import SwiftDate

class SunObject {
    
    var date = DateInRegion(year: Date().year, month: Date().month, day: Date().day)
    var timePeriod = ""
    var scene = ""
    
}
