//
//  ForecastCell.swift
//  DateTests
//
//  Created by Brandon Lambert on 8/13/19.
//  Copyright © 2019 Brandon Lambert. All rights reserved.
//

import UIKit

class ForecastCell: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var lowTempLabel: UILabel!
    @IBOutlet weak var precipitationLabel: UILabel!
    @IBOutlet weak var dayPartNameLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        Bundle.main.loadNibNamed("ForecastCell", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }

}
