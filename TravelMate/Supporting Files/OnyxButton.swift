import AudioToolbox
import UIKit

class OnyxButton: UIButton {
    var pressFeedbackGenerator: UIImpactFeedbackGenerator?
    var depressFeedbackGenerator: UIImpactFeedbackGenerator?
    var soundID: SystemSoundID = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    deinit {
        pressFeedbackGenerator = nil
        depressFeedbackGenerator = nil
        AudioServicesDisposeSystemSoundID(self.soundID)
    }
    
    func press() {
        AudioServicesPlaySystemSound(soundID)
        pressFeedbackGenerator?.impactOccurred()
        pressFeedbackGenerator?.prepare()
    }
    
    func depress() {
        depressFeedbackGenerator?.impactOccurred()
        depressFeedbackGenerator?.prepare()
    }
    
    private func configure() {
        pressFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        pressFeedbackGenerator?.prepare()
        depressFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
        depressFeedbackGenerator?.prepare()
        setSoundId()
    }
    
    private func setSoundId() {
        let soundFile = "/System/Library/Audio/UISounds/nano/TimerWheelMinutesDetent_Haptic.caf"
        if let url = URL(string: soundFile) {
            AudioServicesCreateSystemSoundID(url as CFURL, &soundID)
        }
    }
}
